var app = {
    lastUpdate: null,
    exit: function(e) {
        navigator.app.exitApp();
    },
    getToken: function() {
        return window.localStorage.getItem('token');
    },
    setToken: function(data) {
        var token;
        if (data instanceof Object) {
            token = data.getResponseHeader('Authorization').split(' ')[1];
        } else {
            token = data;
        }
        window.localStorage.setItem('token', token);
    },
    cekToken: function() {

    },
    getUser: function() {
        // var user = {};
        // user.id = window.localStorage.getItem('user_id');
        // user.name = window.localStorage.getItem('user_name');
        // user.email = window.localStorage.getItem('user_email');
        var user = JSON.parse(window.localStorage.getItem('user'));
        return user;
    },
    toast: function(m) {
        return new $.nd2Toast({ // The 'new' keyword is important, otherwise you would overwrite the current toast instance
            message: m, // Required
            ttl: 5000 // optional, time-to-live in ms (default: 3000)
        });
        // window.plugins.toast.showLongBottom(m);
    },
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, this.onDeviceNotReady);
    },
    onDeviceReady: function() {
        console.log('device ready');
        // GPS.bgGeo = window.backgroundGeolocation;
        
        /**
         * This callback will be executed every time a geolocation is recorded in the background.
         */
        // var callbackFn = function(location) {
        //     console.log(location);
        //     // console.log('[js] BackgroundGeolocation callback:  ' + location.latitude + ',' + location.longitude);
        //     window.localStorage.setItem('lat', location.latitude);
        //     window.localStorage.setItem('lng', location.longitude);
        //     window.localStorage.setItem('positionTime', location.time);
        //     backgroundGeolocation.finish();
        // };

        // var failureFn = function(error) {
        //     console.log('BackgroundGeolocation error' + error);
        // };

        // // BackgroundGeolocation is highly configurable. See platform specific configuration options
        // backgroundGeolocation.configure(callbackFn, failureFn, {
        //     desiredAccuracy: 0,
        //     stationaryRadius: 10,
        //     distanceFilter:20, 
        //     locationProvider: backgroundGeolocation.provider.ANDROID_FUSED_LOCATION,
        //     interval: 1000,
        //     fastestInterval: 500,
        //     activitiesInterval : 2000,
        //     debug: true, // <-- enable this hear sounds for background-geolocation life-cycle.
        //     stopOnTerminate: false, // <-- enable this to clear background location settings when the app terminates
        // });

    },
    onDeviceNotReady: function(e) {
        alert(e.message);
    },
    onLoginSubmit: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var data = $(this).serializeArray();
        server.login(data);
    },
    onSignupSubmit: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var data = $(this).serializeArray();
        server.signup(data);
    },
    onPageContainerShow: function(event, ui) {
        var toPage = ui.toPage[0].id;
        var prevPage = ui.prevPage[0] ? ui.prevPage[0].id : null;

        if (toPage == 'detailtempat' || toPage == 'detailwisata') {
            $('#' + toPage + ' header a').prop('href', '#' + prevPage);
        }
        var show = page.onShow;
        var leave = page.onLeave;
        if (show[toPage]) {
            show[toPage](prevPage);
        }
        if (prevPage !== null && leave[prevPage]) leave[prevPage]();
        else if (toPage === 'map-tambah' && prevPage !== 'form-tambah') Tempat.initMapTambah();

    },
    onClickDaftarWisata: function(e) {
        var jenis = $(this).data('jenis');
        page.loader.show();
        query.listwisata(jenis).done(page.render.wisata);
        // Tempat.listTempat(jenis);
    },
    onClickDaftarTempat: function(e) {
        var jenis = $(this).data('tipe');
        console.log('onClickDaftarTempat' + jenis);
        page.loader.show();
        // $.mobile.loading("show");
        // query.listtempat(jenis).done(page.render.tempat);
        Tempat.listTempat(jenis);
    },
    onClickDetailWisata: function(e) {
        var id = $(this).data('id');
        var wisata_id = $(this).data('wisata');
        page.detailwisata.detail(id);
        // $.when.apply($, query.detailwisata(id, wisata_id)).then(function() {
        //     page.detailwisata.init(arguments);
        // });
    },
    onClickDetailTempat: function(e) {
        var id = $(this).data('id');
        var jenis = $(this).data('jenis');
        console.log(jenis);
        // var wisata_id = $(this).data('wisata');

        Tempat.detail(id, jenis);

    },
    onRatingSubmit: function(e) {
        e.preventDefault();
        // e.stopPropagation();
        var data = $(this).serializeArray();
        server.tambahReview(data);
    },
    onClickTambah: function(e) {
        var jenis = $(this).data('jenis');
        Tempat.loadFormTambah(jenis);
    },
    onClickButtonFoto: function(e) {
        var type = $(this).data('type') === 'camera' ? 1 : 2;
        Tempat.foto(type);
    },
    onClickButtonHapusFoto: function(e) {
        var name = $(this).data('name');
        for (var i = 0; i < Tempat.fileFoto.length; i++) {
            if (Tempat.fileFoto[i].name == name) {
                $(this).parent().remove();
                Tempat.fileFoto.splice(i, 1);
                break;
            }
        }
    },
    onTambahSubmit: function(e) {
        e.preventDefault();
        var data = Tempat.collectData(this);
        Tempat.submitTempat(data);
    },
    onClickLogout: function(e) {
        page.loader.show();
        page.loader.hide('login');
        window.localStorage.removeItem('user');
        window.localStorage.removeItem('token');

    },
    onUbahPasswordSubmit: function(e) {
        e.preventDefault();
        page.loader.show();
        var data = $(this).serializeArray();
        $('#ubahpassword .error-card ul').html('');
        $('#ubahpassword .error-card ').hide();
        $('#ubahpassword .ui-error').removeClass('ui-error');
        server.send('ubahpassword', data, function(data, message, jqhr) {
            console.log(data);
            if (jqhr.status === 200) {
                var temp = new model.user(data);
                query.deleteUser().done(function() {
                    temp.save();
                });
                window.localStorage.setItem('user', JSON.stringify(temp));
                $('#ubahpassword .error-card ul').html('');
                $('#ubahpassword .error-card ').hide();
                $('#ubahpassword .ui-error').removeClass('ui-error');
                page.loader.hide('home');
            } else {
                app.toast('terjadi kesalahan, coba lagi');
                page.loader.hide('login');
            }
        }, function(xhr, textStatus, errorThrown) {
            var response = xhr.responseJSON;
            console.log(response);
            var msg = '';
            for (var name in response) {
                $('#ubahpassword input[name="' + name + '"]').parent().addClass('ui-error');
                for (var i = 0; i < response[name].length; i++) {
                    msg += '<li>' + response[name][i] + '</li>';
                }
            }
            $('#ubahpassword .error-card ul').html(msg);
            $('#ubahpassword .error-card ').show();
            page.loader.hide();
        });

    },
    onChangeNewPassword: function(e) {
        var input = $('#ubahpassword input[name="new_password"],#ubahpassword input[name="new_password_confirmation"]');
        if ($(this).val().length > 0) {
            input.prop('required', true);

        } else {
            input.prop('required', false);
        }
    },
    onClickFavorite: function(e) {
        var id = $(this).data('id');
        var fav = $(this).data('favorite') == 1 ? 0 : 1;
        var btn = $(this).children('.zmdi');
        var query = 'update tempats set favorite=' + fav + ' where tempats.id=' + id;
        dao.db.transaction(function(db) {
            db.executeSql(query);
        }, function(err) {
            console.log(err);
        }, function() {
            if (fav) {
                btn.addClass('zmdi-favorite').removeClass('zmdi-favorite-outline').parent().addClass(' clr-btn-red').removeClass('clr-btn-green');
            } else {
                btn.removeClass('zmdi-favorite').addClass('zmdi-favorite-outline').parent().addClass(' clr-btn-green').removeClass('clr-btn-red');
            }
            $('.btn-favorite').data('favorite', fav);
        });
    },
    onResetPasswordSubmit: function(e) {
        e.preventDefault();
        e.stopPropagation();
        page.loader.show();
        $('#resetpassword .success-card,#resetpassword .error-card ').hide();
        var data = $(this).serializeArray();
        server.send('reset', data, function(data, message, jqhr) {
            $('#resetpassword .success-card ul').html(data.status);
            $('#resetpassword .success-card ').show();
            $('#resetpassword input[name="email"]').val('');
            page.loader.hide();
        }, function(xhr, textStatus, errorThrown) {
            $('#resetpassword .error-card ul').html(xhr.responseJSON.status);
            $('#resetpassword .error-card ').show();
            page.loader.hide();
        });
    },

};


$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + app.getToken());
    },
});
app.initialize();

$(document).ready(function() {
    // page.loader.hilang();
    dao.initialize(page.onCreate);
    $(document).on('submit', '#form-login', app.onLoginSubmit);
    $(document).on('submit', '#form-signup', app.onSignupSubmit);
    $(document).on('submit', '#form-rating', app.onRatingSubmit);
    $(document).on('submit', '#form-tambah form', app.onTambahSubmit);
    $(document).on('submit', '#form-ubahpassword', app.onUbahPasswordSubmit);
    $(document).on('submit', '#form-resetpassword', app.onResetPasswordSubmit);
    $("#listview-panel-kiri").listview();
    $("#panel-kiri").panel();
    $(document).on('click', '#exit', app.exit);
    $(document).on('click', '.btn-wisata', app.onClickDaftarWisata);
    $(document).on('click', '.btn-favorite', app.onClickFavorite);
    // $(document).on('click', '#menu-favorite', app.onClickMenuFavorite);
    $(document).on('click', '.btn-tempat', app.onClickDaftarTempat);
    $(document).on('click', '.review-hapus', page.detailwisata.onClickReviewHapus);
    $(document).on('click', '.review-cancel', page.detailwisata.onClickReviewCancel);
    $(document).on('click', '.review-edit', page.detailwisata.onClickReviewEdit);
    $(document).on('click', 'a[href="#detailwisata"]', app.onClickDetailWisata);
    $(":mobile-pagecontainer").on("pagecontainershow", app.onPageContainerShow);
    $(document).on('click', '.tab-review', page.tabReview.init);
    // $(document).on('click', '.tab-map', page.tabMap.init);
    // $(document).on('click', '.tempat-map', Tempat.initMap);
    $(document).on('click', 'a[href="#detailtempat"]', app.onClickDetailTempat);
    $(document).on('click', '#fab_ctn a[href="#map-tambah"]', app.onClickTambah);
    $(document).on('click', '.btn-foto', app.onClickButtonFoto);
    $(document).on('click', '.btn-hapus-foto', app.onClickButtonHapusFoto);
    $(document).on('click', '.add-tiket', Tempat.addTiket);
    $(document).on('change', '#is_tiket', Tempat.changeTiket);
    $(document).on('change', '#ubahpassword input[name="new_password"]', app.onChangeNewPassword);
    $(document).on('click', '.tiket-delete', Tempat.deleteTiket);
    $(document).on('click', '#menu-logout', app.onClickLogout);
    // $(document).on('click', '#btn-foto', function() {
    //     navigator.camera.getPicture(onSuccess, onFail, {
    //         quality: 50,
    //         destinationType: Camera.DestinationType.FILE_URL,
    //         mediaType: Camera.MediaType.PICTURE,
    //         sourceType: Camera.PictureSourceType.CAMERA,

    //     });

    //     function onSuccess(imageURI) {
    //         // console.log(imageURI);

    //         $('#result-image').attr('src', 'data:image/jpeg;base64,' + imageURI);
    //         var url = encodeURI('http://192.168.43.75/laravel/public/mobile/x');
    //         var image = encodeImageUri(document.getElementById('result-image'));
    //         $.ajax({
    //             url: url,
    //             dataType: 'json',
    //             data: {image : imageURI},
    //             type: 'POST',
    //             headers: {
    //                 'Authorization': 'Bearer ' + app.getToken(),
    //             },
    //             success: function(data) {
    //                 console.log(data);
    //             }
    //         });
    //     }

    //     function onFail(message) {
    //         console.log(message);
    //     }
    // });
});