window.GPS = {
	bgGeo: null,
	// event: function() {
	// 	this.bgGeo.onLocation(this.successLocation, this.failLocation);
	// 	// Fired whenever state changes from moving->stationary or vice-versa.
	// 	this.bgGeo.onMotionChange(this.successMoving, this.failLocation);
	// },
	configure: function() {
		this.bgGeo.configure(this.successLocation, this.failLocation, {
			desiredAccuracy: 0,
			stationaryRadius: 10,
			debug: false,
			// distanceFilter: 1,
			stopOnTerminate: true,
			startForeground: true,
			interval: 1000,
			fastestInterval: 500,
			activitiesInterval: 500,
			notificationTitle: 'Tracking Location',
			locationProvider: 0,
		});
	},
	successLocation: function(location, taskId) {
		console.log(location);
		// console.log('[js] BackgroundGeolocation callback:  ' + location.latitude + ',' + location.longitude);
		window.localStorage.setItem('lat', location.latitude);
		window.localStorage.setItem('lng', location.longitude);
		window.localStorage.setItem('positionTime', location.time);
		GPS.bgGeo.finish();
		if (Map.map) {
			var latLng = Map.geolocation.saveLocation(location);
			Map.marker.me(latLng);
		}
	},
	failLocation: function(errorCode) {
		console.log('- Errircode : ' + errorCode);
	},
};