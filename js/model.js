window.model = {
	save: function() {
		var key = [],
			value = [],
			tanda = [],
			def = $.Deferred();
		var name = this.getTableName();
		var data = this;
		for (var k in data) {
			if (data.hasOwnProperty(k) && typeof data[k] !== 'function' && typeof data[k] !== 'object' ) {
				key.push(k);
				value.push(data[k]);
				tanda.push('?');
			}
		}
		dao.db.transaction(
			function(db) {
				var q = 'insert into ' + name + ' (' + key.join(',') + ') values(' + tanda.join(',') + ')';
				// console.log(q);
				// console.log(value);
				db.executeSql(q, value);
			},
			function(r) {
				console.log(r);
				def.reject(r);
			},
			function() {
				console.log('sukses insert ' + name);
				def.resolve();
			}
		);
		return def;
	},
	tempat: function(data) {
		this.id = data.id;
		this.nama = data.nama;
		this.publish = data.publish;
		this.alamat = data.alamat;
		this.latitude = data.latitude;
		this.longitude = data.longitude;
		this.keterangan = data.keterangan;
		this.tempatable_id = data.tempatable_id;
		this.tempatable_type = data.tempatable_type;
		this.created_at = data.created_at;
		this.updated_at = data.updated_at;
		this.user_id = data.user_id;
		this.save = model.save;
		data.tempatable.tempat_id = data.id;
		this.tempatable = new model[data.tempatable_type.split('\\')[2].toLowerCase()](data.tempatable);
		this.gambars = data.gambars || [];
		this.getTableName = function() {
			return 'tempats';
		};
		this.setGambars = function() {
			var self = this;
			var def = $.Deferred();
			dao.select('select * from gambars where tempat_id=' + self.id)
				.done(function(results) {
					self.gambars = results;
					def.resolve();
				});
			return def.promise();
		};
	},
	gambar: function(data) {
		this.id = data.id;
		this.nama = data.nama;
		this.tempat_id = data.tempat_id;
		this.getTableName = function() {
			return 'gambars';
		};
		this.save = model.save;
	},
	user: function(data) {
		this.id = data.id;
		this.name = data.name;
		this.email = data.email;
		this.getTableName = function() {
			return 'users';
		};
		this.save = model.save;
	},
	jeniswisata: function(data) {
		this.id = data.id;
		this.nama = data.nama;
		this.getTableName = function() {
			return 'jeniswisatas';
		};
		this.save = model.save;
	},
	wisata: function(data) {
		this.id = data.id;
		this.waktu_buka = data.waktu_buka;
		this.waktu_tutup = data.waktu_tutup;
		this.is_tiket = data.is_tiket;
		this.pengelola = data.pengelola;
		this.telepon = data.telepon;
		this.jeniswisata_id = data.jeniswisata_id;
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.tikets = data.tikets || [];
		this.fasilitaswisata = data.fasilitaswisata || [];
		this.getTableName = function() {
			return 'wisatas';
		};
	},
	fasilitaswisata: function(data) {
		this.id = data.id;
		this.nama = data.nama;
		this.wisata_id = data.wisata_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'fasilitaswisatas';
		};
	},
	tiket: function(data) {
		this.id = data.id;
		this.operasional = data.operasional;
		this.nama = data.nama;
		this.wisata_id = data.wisata_id;
		this.harga = data.harga;
		this.getTableName = function() {
			return 'tikets';
		};
		this.save = model.save;
	},
	review: function(data) {
		this.id = data.id;
		this.judul = data.judul;
		this.review = data.review;
		this.rating = data.rating;
		this.user_id = data.user_id;
		this.created_at = data.created_at || new Date();
		this.updated_at = data.updated_at || new Date();
		this.save = model.save;
		this.getTableName = function() {
			return 'reviews';
		};
		this.user = new model.user(data.user);
	},
	spbu: function(data) {
		this.id = data.id;
		this.fasilitas = data.fasilitas;
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'spbus';
		};
	},
	bank: function(data) {
		this.id = data.id;
		this.nama = data.nama;
		this.save = model.save;
		this.getTableName = function() {
			return 'banks';
		};
	},
	atm: function(data) {
		this.id = data.id;
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.bank = data.banks.map(function(o) { return o.nama; }).join(', ');
		this.getTableName = function() {
			return 'atms';
		};
	},
	atm_bank: function(data) {
		this.id = data.id;
		this.atm_id = data.atm_id;
		this.bank_id = data.bank_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'atm_bank';
		};
	},
	event: function(data) {
		this.id = data.id;
		this.waktu_mulai = data.waktu_mulai;
		this.waktu_selesai = data.waktu_selesai;
		this.tgl_mulai = convertDate(data.tgl_mulai);
		this.tgl_selesai = convertDate(data.tgl_selesai);
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'events';
		};

	},
	penginapan: function(data) {
		this.id = data.id;
		this.telepon = data.telepon;
		this.harga_min = data.harga_min;
		this.harga_max = data.harga_max;
		this.fasilitas = data.fasilitas;
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'penginapans';
		};
	},
	restoran: function(data) {
		this.id = data.id;
		this.telepon = data.telepon;
		this.waktu_buka = data.waktu_buka;
		this.waktu_tutup = data.waktu_tutup;
		this.harga_max = data.harga_max;
		this.harga_min = data.harga_min;
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.menu = data.menus.map(function(o) { return o.nama; }).join(', ');
		this.getTableName = function() {
			return 'restorans';
		};
	},
	menu: function(data) {
		this.id = data.id;
		this.nama = data.nama;
		this.restoran_id = data.restoran_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'menus';
		};
	},
	kategori: function(data) {
		this.id = data.id;
		this.nama = data.nama;
		this.save = model.save;
		this.getTableName = function() {
			return 'kategoris';
		};
	},
	transportasi: function(data) {
		this.id = data.id;
		this.telepon = data.telepon;
		this.kategori_id = data.kategori_id;
		this.kategori = data.kategori.nama;
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'transportasis';
		};
	},
	favorite: function(data) {
		this.id = data.id;
		this.wisata_id = data.wisata_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'favorites';
		};
	},
	daftarperjalanan: function(data) {
		this.id = data.id;
		this.wisata_id = data.wisata_id;
		this.mulai = data.mulai;
		this.akhir = data.akhir;
		this.save = model.save;
		this.getTableName = function() {
			return 'daftarperjalanan';
		};
	},
	belanja: function(data) {
		this.id = data.id;
		this.jenis = data.jenis;
		this.tempat_id = data.tempat_id;
		this.save = model.save;
		this.getTableName = function() {
			return 'belanjas';
		};
	},

};

function convertDate(date){
	var x = new Date(date);
	var day = x.getDate();
	var month = x.getMonth()+1;
	var year = x.getFullYear();
	return day+'/'+month+'/'+year;
}


