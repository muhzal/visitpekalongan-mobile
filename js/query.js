window.query = {
	jeniswisata: {
		alam: 1,
		religi: 2,
		bus: 3,
		buatan: 4,
	},
	select: function(a) {
		return 'SELECT `tempats`.`id`,`tempats`.`longitude`,`tempats`.`latitude`, `tempats`.`nama`, `tempats`.`alamat`,' +
			'`tempats`.`keterangan`, `gambars`.`nama` as gambar ' +
			'FROM `gambars` , `tempats` , `' + a + 's` ' +
			'where ' + a + 's.id = tempats.tempatable_id and gambars.tempat_id = tempats.id GROUP by ' + a + 's.id';
	},
	wisata : function(a){
		return 'SELECT `tempats`.*, `gambars`.`nama` as gambar FROM `gambars` , `tempats` , `wisatas`'+
		 ' where wisatas.id = tempats.tempatable_id and tempats.tempatable_type = "App\\Models\\Wisata" and wisatas.jeniswisata_id=' + a + ' and gambars.tempat_id = tempats.id GROUP by wisatas.id';
	},
	semuawisata : function(){
		var path = "App\\Models\\Wisata";
		return 'SELECT wisatas.id as wisata_id, `tempats`.*, `gambars`.`nama` as gambar FROM `gambars` , `tempats` , `wisatas`'+
		 ' where tempats.tempatable_id = wisata_id and tempats.tempatable_type = "App\\Models\\Wisata" and gambars.tempat_id = tempats.id GROUP by wisatas.id';
	},
	marker: function() {
		var q = "SELECT tempats.id,alamat,latitude,longitude,tempats.nama,gambars.nama as gambar,tempatable_type,keterangan from tempats,gambars where"+
					" tempats.id = gambars.tempat_id group by tempats.id";
		return dao.list(q);
	},
	listwisata: function(jenis) {
		var jenis_id = this.jeniswisata[jenis];
		var query = jenis == 'semuawisata' ? this.semuawisata() : this.wisata(jenis);
		console.log(query);
		return dao.list(query);
	},
	listtempat: function(jenis) {
		var q = this.select(jenis);
		return dao.list(q);
	},
	findwisata : function(tempat_id){
		var query = "select * from wisatas,tempats where tempats.id="+tempat_id+" and tempats.tempatable_id=wisatas.id";	
		return dao.find(query);
	},
	gambars : function(tempat_id){
		var gambar = "select * from gambars where tempat_id="+tempat_id;
		return dao.many(gambar);
	},
	tikets : function(wisata_id){
		var tikets = "select * from tikets where wisata_id="+wisata_id;
		return dao.many(tikets);
	},
	fasilitaswisatas : function(wisata_id){
		var fasilitaswisatas = "select * from fasilitaswisatas where wisata_id="+wisata_id;
		return dao.many(fasilitaswisatas);
	},
	detailwisata : function(tempat_id,wisata_id){
		var result = [];
		result.push(this.findwisata(tempat_id));
		result.push(this.gambars(tempat_id));
		result.push(this.tikets(wisata_id));
		result.push(this.fasilitaswisatas(wisata_id));
		return result;
	},
	user : function(){
		var query = 'select * from users';
		return dao.find(query);
	},
	deleteUser : function(){
		var query = 'delete from users';
		return dao.delete(query);
	},
	userReview : function(){		
		var query = 'select * from reviews';
		return dao.find(query);
	},
};