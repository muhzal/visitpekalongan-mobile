/* globals $:false, google:false,app:false */
window.Map = {};
if (typeof google !== 'undefined') {
	Map = {
		init: function(id, opt) {
			id = id || 'peta-canvas';
			opt = opt || this.options.map();
			this.map = new google.maps.Map(document.getElementById(id), opt);
			// Map.geolocation.current();
			// Map.geolocation.watch();
		},
		destroy: function() {
			console.log('map destroyed');
			if (Map.map) {
				$(Map.map.getDiv()).children().remove();
				Map.map = null;
				Map.marker.currentMarker = null;
				Map.marker.markers = [];
				Map.geolocation.clearWatch(Map.geolocation.watchId);
			}
		},
		latLng: function(lat, lng) {
			lat = lat || 0;
			lng = lng || 0;
			return new google.maps.LatLng(lat, lng);
		},
		options: {
			map: function() {
				var opt = {};
				opt.center = Map.latLng(-6.9486877, 109.5421919);
				opt.zoom = 12;
				opt.disableDoubleClickZoom = true;
				opt.streetViewControl = false;
				opt.zoomControlOptions = {
					position: google.maps.ControlPosition.LEFT_CENTER,
					style: google.maps.ZoomControlStyle.SMALL,
				};
				return opt;
			},
			icon: {
				wisata: 'img/icon/wisata.png',
				restoran: 'img/icon/restoran.png',
				atm: 'img/icon/atm.png',
				spbu: 'img/icon/spbu.png',
				penginapan: 'img/icon/penginapan.png',
				transportasi: 'img/icon/wisata.png',
				belanja: 'img/icon/belanja.png',
				event: 'img/icon/event.png',
				me: 'img/icon/me.png',
			},
			marker: function(latLng, data) {
				var tipe = data ? data.tempatable_type.split('\\')[2].toLowerCase() : 'me';
				var url = this.icon[tipe];
				return {
					position: latLng,
					icon: {
						url: url,
						size: new google.maps.Size(30, 35),
						origin: new google.maps.Point(0, 0),
						anchor: new google.maps.Point(13, 34)
					},
				};
			},
			infoWindow: function(data) {
				var jenis = data.tempatable_type ? data.tempatable_type.split('\\')[2].toLowerCase() : null;
				var url = jenis === 'wisata' ? '#detailwisata' : '#detailtempat';
				var dom = "<h3>" + data.nama + "</h3>" +
					"<img src='" + server.url + "img/small/" + data.gambar + "' align='left'><br>" +
					"<b>Alamat</b> : <p>" + data.alamat + "</p>" +
					"<a href='" + url + "' data-id='" + data.id + "' data-jenis='" + jenis + "' data-transition='slide'>lihat detail..</a>";
				return {
					content: dom,
					title: data.nama
				};
			},
			route: function(desti, ori, wp) {
				var a = {};
				var temp = Map.geolocation.getCurrent();
				a.origin = ori || Map.latLng(temp.lat, temp.lng);
				a.destination = desti || Map.latLng(temp.lat, temp.lng);
				// a.waypoints = wp || [];
				// a.optimizeWaypoints = true;
				a.travelMode = google.maps.TravelMode.DRIVING;
				return a;
			},
		},
		marker: {
			markers: [],
			onClick: function(marker, data) {
				marker.addListener('click', function() {
					Map.marker.infoWindow(data).open(Map.map, marker);
				});
			},
			infoWindow: function(data) {
				var opt = Map.options.infoWindow(data);
				var infoWindow = new google.maps.InfoWindow(opt);
				return infoWindow;
			},
			create: function(latLng, data) {
				var marker = new google.maps.Marker(Map.options.marker(latLng, data));
				if (data) {
					this.onClick(marker, data);
				}
				return marker;
			},
			add: function(latLng, data) {
				var marker = Map.marker.create(latLng, data);
				marker.setMap(Map.map);
				Map.marker.markers.push(marker);
				// console.log(this.markers);
			},
			me: function(latLng) {
				var marker = Map.marker;
				latLng = Map.latLng(latLng.lat, latLng.lng);
				if (marker.currentMarker) {
					marker.currentMarker.setPosition(latLng);
				} else {
					marker.currentMarker = marker.create(latLng);
					marker.currentMarker.setMap(Map.map);
				}
				// Map.map.setZoom(13);
				// Map.map.setCenter(latLng);

			},
			remove: function() {
				this.hide();
				this.marker = [];
			},
			hide: function() {
				for (var i = 0; i < this.marker.length; i++) {
					this.marker[i].setMap(null);
				}
			},
			show: function() {
				for (var i = 0; i < this.marker.length; i++) {
					this.marker[i].setMap(Map.map);
				}
			}
		},
		direction: {
			route: function(a) {
				var opt = a || Map.options.route();
				// if (opt.origin === null && opt.destination) {
				// 	console.log('origin dan destination blm di set');
				// 	return false;
				// }
				this.service = new google.maps.DirectionsService();
				this.display = new google.maps.DirectionsRenderer({
					suppressMarkers: true,
				});
				this.display.setMap(Map.map);
				return this.startRoute(opt);
			},
			startRoute: function(opt) {
				var self = this;
				var def = $.Deferred();
				self.service.route(opt, function(response, status) {
					// console.log(response);
					if (status == google.maps.DirectionsStatus.OK) {
						self.display.setDirections(response);
						def.resolve(response);
					} else {
						// console.log('directionsService status: ' + status);
						def.reject(status);
					}
				});
				return def.promise();
			},
			jaraktotal: function(response) {
				var route = response.routes[0];
				var totalJarak;
				for (var i = 0; i < route.legs.length; i++) {
					totalJarak += route.legs[i].distance.value;
				}
				return totalJarak / 1000; //dari meter ke km
			}

		},
	};
} else {
	console.log('tidak dapat memuat google maps');
	// app.toast('tidak dapat memuat peta');
	Map.options = {};
}

Map.options.geolocation = {
	enableHighAccuracy: true,
	timeout: 5000,
	maximumAge: Infinity,
};
Map.geolocation = {
	getSavePosition: function() {
		return {
			lat: window.localStorage.getItem('lat'),
			lng: window.localStorage.getItem('lng')
		};
	},
	savePosition: function(position) {
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		var time = position.timestamp;
		window.localStorage.setItem('lat', lat);
		window.localStorage.setItem('lng', lng);
		window.localStorage.setItem('positionTime', time);
		return {
			'lat': lat,
			'lng': lng
		};
	},
	saveLocation: function(location) {
		var lat = location.latitude;
		var lng = location.longitude;
		var time = location.time;
		window.localStorage.setItem('lat', lat);
		window.localStorage.setItem('lng', lng);
		window.localStorage.setItem('time', time);
		return {
			'lat': lat,
			'lng': lng
		};
	},
	current: function() {
		var optGeol = Map.options.geolocation;
		var geo = Map.geolocation;
		console.log('current proses');
		navigator.geolocation.getCurrentPosition(geo.currentSuccess, geo.currentError, optGeol);
	},
	currentSuccess: function(position) {
		console.log('current success');
		var latLng = Map.geolocation.savePosition(position);

		if (Map.map && page.activePage().prop('id') != 'map-tambah')
			Map.marker.me(latLng);
	},
	currentError: function(e) {
		console.log('gagal posisi');
		console.log(e);
		app.toast('Gagal mendapatkan lokasi saat ini');
	},
	getCurrent: function() {
		var geo = Map.geolocation;
		geo.watch(true);
		// $.mobile.loading('show');
		setTimeout(function() {
			if (geo.getUserLatLng() === null) {
				app.toast('Lokasi saat ini tidak dapat ditemukan...');
			}
			// $.mobile.loading('hide');
			geo.clearWatch();
		}, 10000);
		return geo.getUserLatLng();
	},
	getUserLatLng: function() {
		var lat = window.localStorage.getItem('lat');
		var lng = window.localStorage.getItem('lng');
		var latLng = lat === null && lng === null ? null : {
			lat: lat,
			lng: lng
		};
		return latLng;
	},
	watch: function(clear) {
		var options = Map.options.watch;
		var geo = Map.geolocation;
		// console.log('watching');
		if (clear)
			this.clearWatch();
		Map.geolocation.watchId = navigator.geolocation.watchPosition(geo.watchSuccess, geo.watchError, options);
	},
	watchSuccess: function(pos) {
		Map.geolocation.currentSuccess(pos);
		// Map.geolocation.clearWatch();
	},
	watchError: function(e) {
		Map.geolocation.currentError(e);
	},
	clearWatch: function() {
		var id = Map.geolocation.watchId;
		if (id) navigator.geolocation.clearWatch(id);
	},
	userPosition: function() {
		var option = Map.options.geolocation,
			def = $.Deferred(),
			watchId;
		watchId = navigator.geolocation.watchPosition(function(pos) {
			def.resolve(pos, watchId);
		}, function(err) {
			console.log(err);
			def.reject(err);
		});
		return def.promise();
	},
};
Map.distance = {
	proses: function(data) {
		var result = [];
		var def = $.Deferred();
		// var current = Map.geolocation.getCurrent() || null;
		var self = Map.distance;
		var save = Map.geolocation.getSavePosition();
		Map.distance.userPosition().done(function(pos) {
			result = self.prosesData(data, pos);
			def.resolve(result);
		}).fail(function(e) {
			var save = Map.geolocation.getSavePosition();
			if (save.lat || save.lng) {
				result = self.prosesData(data, save);
			} else {
				$.each(data, function(i, v) {
					result.push(v);
				});
			}
			def.resolve(result);
		});
		// GPS.bgGeo.getLocations(function(array) {
		// 	if (array.length) {
		// 		var location = array[array.length - 1];
		// 		var loc = {
		// 			lat: location.latitude,
		// 			lng: location.longitude
		// 		};
		// 		result = self.prosesData(data, loc);
		// 		def.resolve(result);
		// 	} else {
		// 		Map.distance.userPosition().done(function(pos) {
		// 			result = self.prosesData(data, pos);
		// 			def.resolve(result);
		// 		}).fail(function(e) {
		// 			var save = Map.geolocation.getSavePosition();
		// 			if (save.lat || save.lng) {
		// 				result = self.prosesData(data, save);
		// 			} else {
		// 				$.each(data, function(i, v) {
		// 					result.push(v);
		// 				});
		// 			}
		// 			def.resolve(result);
		// 		});
		// 	}
		// }, function(e) {
		// 	if (save.lat || save.lng) {
		// 		result = self.prosesData(data, save);
		// 	} else {
		// 		$.each(data, function(i, v) {
		// 			result.push(v);
		// 		});
		// 	}

		// 	def.resolve(result);
		// });

		// if (current === null) {

		// } 
		// else {
		// 	result = self.prosesData(data, current);
		// 	def.resolve(result);
		// }

		return def.promise();
	},
	prosesData: function(data, pos) {
		var temp;
		var self = Map.distance;
		var result = [];
		for (var i = 0; i < data.length; i++) {
			temp = data[i];
			temp.jarak = self.hitung(pos.lat, pos.lng, temp.latitude, temp.longitude);
			result.push(temp);
		}
		return result;
	},
	hitung: function(lat1, lng1, lat2, lng2) {
		var R = 6371; // Radius of the earth in km
		var dLat = this.deg2rad(parseFloat(lat2 - lat1)); // this.deg2rad below
		var dlng = this.deg2rad(parseFloat(lng2 - lng1));
		var a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
			Math.sin(dlng / 2) * Math.sin(dlng / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c; // Distance in km

		return d;
	},
	deg2rad: function(deg) {
		return deg * (Math.PI / 180);
	},
	userPosition: function() {
		var geo = Map.geolocation;
		var def = $.Deferred();
		// var watchId;
		// watchId = navigator.geolocation.watchPosition(function(pos) {
		// 	def.resolve(geo.savePosition(pos));
		// 	navigator.geolocation.clearWatch(watchId);
		// }, function(e) {
		// 	def.reject(e);
		// }, Map.options.geolocation);
		navigator.geolocation.getCurrentPosition(function(pos) {
			def.resolve(geo.savePosition(pos));
		}, function(e) {
			def.reject(e);
		}, Map.options.geolocation);

		return def.promise();
	}
};
Map.options.watch = {
	enableHighAccuracy: true,
	timeout: Infinity,
	maximumAge: Infinity,
};