window.dao = {
    initialize: function(callback) {
        var device = { platform: "browser"};
        if (device.platform === 'Android' || device.platform === 'Ios') {
            this.db = window.sqlitePlugin.openDatabase({
                name: 'vp.db',
                location: 'default',
                androidLockWorkaround: 1
            }, function(db) {
                console.log('database sqlite opened');
                if (window.localStorage.getItem('installed') === null) {
                    dao.initTable(callback);
                } else {
                    callback();
                }
            }, this.txErrorHandler);
        } else if (window.openDatabase && device.platform === 'browser') {
            console.log('database websql opened');
            this.db = window.openDatabase('vp.db', '1.0', 'Visit Pekalongan DB', 2 * 1024 * 1024);
            if (window.localStorage.getItem('installed') === null) {
                dao.initTable(callback);
            } else {
                callback();
            }
        }
    },
    initTable: function(callback) {
        this.db.transaction(function(tx) {
            console.log('init table');
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'users'('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'name' varchar(50) DEFAULT NULL,'email' varchar(255) DEFAULT NULL)", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'tempats' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE, 'publish' tinyint(1) DEFAULT 1,'user_id' int(10) NOT NULL, 'favorite' tinyint(1) DEFAULT 0,'nama' varchar(50) NOT NULL,'alamat' varchar(255) DEFAULT NULL,'tempatable_id' int(10) NOT NULL,'tempatable_type' varchar(255) NOT NULL,'latitude' double DEFAULT 0,'longitude' double DEFAULT 0,'keterangan' longtext DEFAULT 'Tidak Ada Keterangan','created_at' timestamp DEFAULT CURRENT_TIMESTAMP,'updated_at' timestamp DEFAULT CURRENT_TIMESTAMP )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'gambars' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'nama' varchar(255) DEFAULT NULL,'tempat_id' int(10) NOT NULL, CONSTRAINT 'gambars_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'spbus' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'fasilitas' varchar(255) DEFAULT NULL,'tempat_id' int(10) NOT NULL, CONSTRAINT 'spbus_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'atms' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'bank' varchar(1000) DEFAULT NULL, 'tempat_id' int(10) NOT NULL, CONSTRAINT 'atms_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'banks' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'nama' varchar(255) DEFAULT NULL )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'atm_bank' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'atm_id' int(10) NOT NULL,'bank_id' int(10) NOT NULL, CONSTRAINT 'atm_bank_bank_id_foreign' FOREIGN KEY ('bank_id') REFERENCES 'banks' ('id') ON DELETE CASCADE,CONSTRAINT 'atm_bank_atm_id_foreign' FOREIGN KEY ('atm_id') REFERENCES 'atms' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'belanjas' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'jenis' varchar(255) DEFAULT NULL,'tempat_id' int(10) NOT NULL, CONSTRAINT 'belanjas_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'events' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'waktu_mulai' TIME DEFAULT CURRENT_TIME,'waktu_selesai' TIME DEFAULT CURRENT_TIME,'tgl_mulai' DATE DEFAULT CURRENT_DATE,'tgl_selesai' DATE DEFAULT CURRENT_DATE,'tempat_id' int(10) NOT NULL, CONSTRAINT 'events_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'penginapans' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'telepon' int(11) DEFAULT 0,'harga_min' int(11) DEFAULT 0,'harga_max' int(11) DEFAULT 0,'fasilitas' varchar(255) DEFAULT NULL,'tempat_id' int(10) NOT NULL, CONSTRAINT 'penginapans_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'restorans' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'menu' varchar(1000) DEFAULT 'Tidak Ada Menu', 'waktu_buka' time DEFAULT CURRENT_TIME,'waktu_tutup' time DEFAULT CURRENT_TIME,'telepon' int(11) DEFAULT 0,'harga_min' int(11) DEFAULT 0,'harga_max' int(11) DEFAULT 0,'tempat_id' int(10) NOT NULL, CONSTRAINT 'restorans_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'menus' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'nama' varchar(255) DEFAULT NULL,'restoran_id' int(10) NOT NULL, CONSTRAINT 'menus_restoran_id_foreign' FOREIGN KEY ('restoran_id') REFERENCES 'restorans' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'kategoris' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'nama' varchar(255) DEFAULT NULL )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'transportasis' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'telepon' varchar(255) DEFAULT NULL,'kategori' varchar(255) DEFAULT NULL,'kategori_id' int(10) NOT NULL,'tempat_id' int(10) NOT NULL, CONSTRAINT 'transportasis_kategori_id_foreign' FOREIGN KEY ('kategori_id') REFERENCES 'kategoris' ('id') ON DELETE CASCADE,CONSTRAINT 'transportasis_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'jeniswisatas' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'nama' varchar(255) DEFAULT NULL )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'wisatas' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE, 'waktu_buka' time DEFAULT CURRENT_TIME, 'waktu_tutup' time DEFAULT CURRENT_TIME, 'is_tiket' tinyint(1) DEFAULT 0, 'telepon' varchar(255) DEFAULT NULL, 'pengelola' varchar(255) DEFAULT NULL, 'jeniswisata_id' int(10) NOT NULL, 'tempat_id' int(10) NOT NULL, CONSTRAINT 'wisatas_jeniswisata_id_foreign'FOREIGN KEY ('jeniswisata_id') REFERENCES 'jeniswisatas' ('id') ON DELETE CASCADE , CONSTRAINT 'wisatas_tempat_id_foreign'FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'fasilitaswisatas' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'nama' varchar(255) DEFAULT NULL,'wisata_id' int(10) NOT NULL, CONSTRAINT 'fasilitaswisatas_wisata_id_foreign' FOREIGN KEY ('wisata_id') REFERENCES 'wisatas' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'reviews' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'judul' varchar(50) DEFAULT NULL,'rating' int(11) DEFAULT 0,'review' varchar(255) DEFAULT NULL,'user_id' int(10) NOT NULL,'wisata_id' int(10) NOT NULL,'created_at' timestamp DEFAULT CURRENT_TIMESTAMP,'updated_at' timestamp DEFAULT CURRENT_TIMESTAMP, CONSTRAINT 'reviews_wisata_id_foreign' FOREIGN KEY ('wisata_id') REFERENCES 'wisatas' ('id') ON DELETE CASCADE,CONSTRAINT 'reviews_user_id_foreign' FOREIGN KEY ('user_id') REFERENCES 'users' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'tikets' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'nama' varchar(255) DEFAULT NULL,'operasional' varchar(255) DEFAULT NULL,'harga' int(11) DEFAULT 0,'wisata_id' int(10) NOT NULL, CONSTRAINT 'tikets_wisata_id_foreign' FOREIGN KEY ('wisata_id') REFERENCES 'wisatas' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'favorites' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'tempat_id' int(10) NOT NULL, CONSTRAINT 'favorites_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS 'daftarperjalanan' ('id' int(10) PRIMARY KEY ON CONFLICT REPLACE,'tempat_id' int(10) NOT NULL, 'awal' tinyint(1) DEFAULT 0, 'akhir' tinyint(1) DEFAULT 0,CONSTRAINT 'transportasis_tempat_id_foreign' FOREIGN KEY ('tempat_id') REFERENCES 'tempats' ('id') ON DELETE CASCADE )", []);
            tx.executeSql("INSERT INTO 'jeniswisatas' ('id','nama') values('1','Wisata Alam')");
            tx.executeSql("INSERT INTO 'jeniswisatas' ('id','nama') values('2','Wisata Religi')");
            tx.executeSql("INSERT INTO 'jeniswisatas' ('id','nama') values('3','Wisata Budaya dan Sejarah')");
            tx.executeSql("INSERT INTO 'jeniswisatas' ('id','nama') values('4','Wisata Buatan')");
        }, function(e) {
            console.log(e);
        }, function() {
            window.localStorage.setItem('installed',true);
            callback();
        });
    },
    gagalInstall: function() {
        alert('gagal install');
    },
    transaction: function(query, data, callback) {
        this.db.transaction(function(tx) {
            tx.executeSql(query, data);
        }, callback, this.txErrorHandler);
    },
    executeSql: function(query, data, callback) {
        this.db.executeSql(query, data, callback, this.txErrorHandler);
    },
    insert: function(query, data, callback) {
        console.log('insert');
        console.log(query);
        console.log(data);
        this.db.transaction(
            function(db) {
                db.executeSql(query, data);
            },
            function(r) {
                console.log(r);
                console.log('success');
                callback(r);
            },
            this.txErrorHandler
        );
    },
    update: function(query, data, callback) {
        this.db.transaction(
            function(db) {
                db.executeSql(query, data);
            },
            function(r) {
                callback(r);
            },
            this.txErrorHandler
        );
    },
    select: function(query) {
        var def = $.Deferred();
        var result = [];

        dao.db.readTransaction(function(tx) {
            tx.executeSql(query, [], function(tx, results) {
                for (var i = 0; i < results.rows.length; i++) {
                    result.push(results.rows.item(i));
                }
                def.resolve(result);
            }, this.txErrorHandler);
        }, this.txErrorHandler, function() {

        });

        return def.promise();
    },
    find: function(query) {
        var def = $.Deferred();
        dao.db.readTransaction(function(tx) {
            tx.executeSql(query, [], function(tx, results) {
                var hasil = results.rows.length ? results.rows.item(0) : null;
                def.resolve(hasil);
            }, function(e) {
                def.reject(e);
            });
        }, function(e) {
            console.log(e);
        }, function() {
        });
        return def.promise();
    },
    many: function(query) {
        var def = $.Deferred(), hasil=[];
        dao.db.readTransaction(function(tx) {
            tx.executeSql(query, [], function(tx, results) {
                for (var i = 0; i < results.rows.length; i++) {
                    hasil.push(results.rows.item(i));
                }
                def.resolve(hasil);
            }, function(e) {

                def.reject(e);
            });
        }, function(e) {
            console.log(e);
        }, function() {
        });
        return def.promise();
    },
    delete : function(query){
        var def = $.Deferred();
        dao.db.transaction(function(tx){
            tx.executeSql(query);
        },function(tx){ //fail      
            console.log(tx.message);
            console.log(tx);      
        },function(){ //sukses
            def.resolve();
        });
        return def.promise();
    },
    list: function(query) {
        var def = $.Deferred();
        dao.db.readTransaction(function(tx) {
            tx.executeSql(query, [], function(tx2, results) {                
                var hasil = [];
                for (var i = 0; i < results.rows.length; i++) {
                    hasil.push(results.rows.item(i));
                }
                def.resolve(hasil);
            }, dao.txErrorHandler);
        });
        return def.promise();
    },
    txErrorHandler: function(tx,x) {        
        console.log(x);
    },
    getLastUpdate : function(){
        var def = $.Deferred();
        dao.db.readTransaction(function(tx){
            tx.executeSql('SELECT UPDATEd_AT from tempats order by UPDATEd_at DESC limit 1',[],function (tx2,res) {                 
                 var hasil = res.rows.length > 0 ? res.rows.item(0).updated_at : null;
                 def.resolve(hasil);
            });
        },function(x){
            console.log(x);
        });
        return def.promise();
    },
};