    window.page = {
        onShow: {
            peta: function() {
                if (typeof google !== 'undefined') {
                    Map.init();
                    if (dao.db) {
                        query.marker().done(function(data) {
                            var a, b;
                            for (var i = 0; i < data.length; i++) {
                                a = data[i];
                                b = Map.latLng(a.latitude, a.longitude);
                                Map.marker.add(b, a);
                            }
                        });
                    }
                } else {
                    app.toast('anda tidak terhubung ke internet');
                    app.toast('tidak dapat memuat peta');
                }
            },
            detailwisata: function(prev) {
                $('#detailwisata-back').attr('href', '#' + prev);
            },
            signup: function() {
                $('.error-card').hide();
                $('.error-card ul li').remove();
            },
            ubahpassword: function() {
                var user = app.getUser();
                $('#form-ubahpassword input[name="name"]').val(user.name);
                $('#form-ubahpassword input[name="email"]').val(user.email);
                $('#form-ubahpassword input[name="user_id"]').val(user.id);
                $('#form-ubahpassword input[type="password"]').val('');
            },
            home: function() {

            },
        },
        onLeave: {
            peta: function() {
                if (Map.destroy) {
                    Map.destroy();
                }
            },
            resetpassword: function() {
                $('#resetpassword .error-card ul, #resetpassword .success-card ul').html("");
                $('#resetpassword .error-card, #resetpassword .success-card').hide();
            },
            // detailwisata: function() {
            //     $('.sudah-rating ul.ui-listview li').html('');
            //     $('#detailwisata').removeClass('loaded');
            //     $('#hasil-rating').addClass('proses-rating');
            //     if (page.detailwisata.objswiper)
            //         page.detailwisata.objswiper.destroy(true, true);
            //     $('.swiper-container .swiper-wrapper').children().remove();
            //     if (page.detailwisata.rateYo2)
            //     // page.detailwisata.rateYo2.rateYo('rating', 0);
            //         page.detailwisata.rateYo2.rateYo('destroy');
            //     if (page.detailwisata.rateYo)
            //         page.detailwisata.rateYo.rateYo('rating', 0);
            //     // page.detailwisata.rateYo.rateYo('destroy');
            //     Map.geolocation.clearWatch();
            //     $('.tab-info').click();
            //     $('#form-rating').hide();
            //     page.detailwisata.resetFormUserReview();
            //     if (Map.destroy) {
            //         Map.destroy();
            //     }
            // }
        },
        onCreate: function() {
            console.log('onCreate');
            server.cekUpdate();
            Map.geolocation.watch();         
        },
        render: {
            proses: function(data, id, jenis) {

                $(id).children().remove();

                this.urutkan(data).done(function(hasil) {
                    page.render.setListToDom(hasil, id, jenis);
                }).fail(function(e) {
                    app.toast('Lokasi Anda Tidak dapat ditemukan');
                    console.log(e);
                    page.render.setListToDom(data, id, jenis);
                });
            },
            setListToDom: function(data, id, jenis) {
                var dom = '';
                for (var i = 0; i < data.length; i++) {
                    dom += page.listview[jenis](data[i]);
                }
                page.listview.initList(id, dom);
                // $.mobile.loading("hide");
                page.loader.hide();
            },
            wisata: function(data) {
                var id = '#list-wisata',
                    jenis = 'wisata';
                page.render.proses(data, id, jenis);
            },
            tempat: function(data) {
                var id = '#list-tempat',
                    jenis = 'tempat';
                page.render.proses(data, id, jenis);
            },
            urutkan: function(data) { //data object dari database                
                var def = $.Deferred();
                Map.distance.proses(data).done(function(temp) {
                    temp.sort(function(a, b) {
                        return parseFloat(a.jarak) - parseFloat(b.jarak);
                    });
                    def.resolve(temp);
                }).fail(function(e) {
                    def.reject(e);
                });
                return def.promise();
            },
        },
        listview: {
            initList: function(id, dom) {
                $(id).append(dom);
                $(id).listview('refresh');
            },
            wisata: function(data) {
                // console.log(data);
                data.jarak = data.jarak || 0;
                var fav = data.favorite ? 'zmdi-favorite' : 'zmdi-favorite-outline';
                var jenis = data.tempatable_type ? data.tempatable_type.split('\\')[2].toLowerCase() : null;
                var gambar = data.gambar === null ? '404.png' : data.gambar;
                console.log(jenis);
                console.log(data);
                var url = jenis === 'wisata' ? '#detailwisata' : '#detailtempat';
                return '<li>' +
                    '<a href="' + url + '" data-id="' + data.id + '" data-jenis="' + jenis + '"class="ui-btn waves-effect waves-button waves-effect waves-button"  data-transition="slide">' +
                    '<img src="' + server.url + 'img/small/' + gambar + '"  width="100px" height="100px" class="ui-thumbnail ui-thumbnail-circular" />' +
                    '<h2>' + data.nama + '</h3><p>' + data.alamat + '<br>' + // data.keterangan +
                    '</p>' +
                    '<p><strong>' + parseFloat(data.jarak).toFixed(2) + '</strong> km</p>' +
                    '</a>' +
                    '<button class="clr-red ui-li-aside ui-mini ui-btn-transparent"><i class="zmdi ' + fav + '"></i></button>' +
                    '</li>';
            },
            tempat: function(data) {
                data.jarak = data.jarak || 0;
                return '<li>' +
                    '<a href="#detail" data-id="' + data.id + '" class="ui-btn waves-effect waves-button waves-effect waves-button"  data-transition="slide">' +
                    '<img src="' + server.url + 'img/small/' + data.gambar + '"  width="100px" height="100px" class="ui-thumbnail ui-thumbnail-circular" />' +
                    '<h2>' + data.nama + '</h3><p>' + data.alamat + '<br>' + data.keterangan + '</p><p class="ui-li-aside"><strong>' + parseFloat(data.jarak).toFixed(2) + '</strong> km</p>' +
                    '</a>' +
                    '</li>';
            },

        },
        activePage: function() {
            return $(":mobile-pagecontainer").pagecontainer('getActivePage');
        },
        change: function(page) {
            $(":mobile-pagecontainer").pagecontainer("change", "#" + page, {
                showLoadMsg: false
            });
        },
        loader: {
            tampil: function() {
                $('body').removeClass('loaded');
            },
            hilang: function() {
                $('body').addClass('loaded');
            },
            show: function() {
                $('body').removeClass('loaded');
            },
            hide: function(to) {
                if (to) page.change(to);
                $('body').addClass('loaded');
            }
        },
        detailwisata: {
            detail: function(id) {
                var self = this;
                $('#detailwisata .tab-info').click();

                self.getData(id).done(function(data) {
                    console.log(data);
                    // self.init(data)
                    self.currentData = data;
                    page.tabMap.data = data;
                    self.loadDetailPage(data);
                    $('#detailwisata .btn-favorite').data({
                        'id': id,
                        'favorite': data.favorite
                    });
                    if (data.favorite) {
                        $('#detailwisata .btn-favorite .zmdi').addClass('zmdi-favorite').removeClass('zmdi-favorite-outline').parent().addClass('clr-btn-red').removeClass('clr-btn-green');
                    } else {
                        $('#detailwisata .btn-favorite .zmdi').removeClass('zmdi-favorite').addClass('zmdi-favorite-outline').parent().addClass('clr-btn-green').removeClass('clr-btn-red');
                    }
                    $(document).one('click', '#detailwisata .tab-map', self.initMap);
                }).fail(function(err) {
                    console.log(err);
                });
            },
            getData: function(id) {
                var def = $.Deferred();
                var tempats = 'SELECT tempats.*,wisatas.*,jeniswisatas.nama as jeniswisata from wisatas join jeniswisatas on jeniswisatas.id=wisatas.jeniswisata_id join tempats on tempats.id=wisatas.tempat_id  where tempats.id=' + id;
                var tikets = 'select * from tikets where wisata_id in (select id from wisatas where tempat_id=' + id + ')';
                var fasilitas = 'select * from fasilitaswisatas where wisata_id in (select id from wisatas where tempat_id=' + id + ')';
                var gambars = 'select * from gambars where tempat_id=' + id;
                var objTempat = {},
                    objTiket = [],
                    objGambar = [],
                    objFasilitas = [];
                dao.db.readTransaction(function(tx) {
                    tx.executeSql(tempats, [], function(tx2, res) {
                        // console.log(res.rows);
                        objTempat = res.rows.length > 0 ? res.rows.item(0) : {};
                    });
                    tx.executeSql(tikets, [], function(tx2, res) {
                        // console.log(res.rows);
                        for (var i = 0; i < res.rows.length; i++) {
                            objTiket.push(res.rows.item(i));
                        }
                    });
                    tx.executeSql(fasilitas, [], function(tx2, res) {
                        // console.log(res.rows);
                        for (var i = 0; i < res.rows.length; i++) {
                            objFasilitas.push(res.rows.item(i));
                        }
                    });
                    tx.executeSql(gambars, [], function(tx2, res) {
                        // console.log(res.rows);
                        for (var i = 0; i < res.rows.length; i++) {
                            objGambar.push(res.rows.item(i));
                        }
                    });
                }, function(err) {
                    def.reject(err);
                }, function() {
                    objTempat.tikets = objTiket;
                    objTempat.fasilitas = objFasilitas;
                    objTempat.gambar = objGambar;
                    def.resolve(objTempat);
                });
                return def.promise();
            },
            initMap: function() {
                var data = page.detailwisata.currentData || {};
                if (typeof google !== 'undefined') {

                    $('#detailwisata .wisata-map').html('<div id="wisata-map"></div>');
                    var optMap = Map.options.map();
                    var latLng = Map.latLng(data.latitude, data.longitude);
                    var height = window.screen.height;
                    var header = $("#detailwisata header").height();
                    $('#wisata-map').height(height - header);
                    optMap.center = latLng;
                    // opt.zoom = 11;
                    Map.init('wisata-map', optMap);
                    Map.marker.create(latLng, data).setMap(Map.map);
                    // page.tabMap.route(latLng);
                    var pos = Map.geolocation.getSavePosition();
                    if (pos.lat || pos.lng) {
                        var ori = Map.latLng(pos.lat, pos.lng);
                        var optRoute = Map.options.route(latLng, ori);
                        Map.direction.route(optRoute);
                        Map.marker.me(pos);
                    }


                    // Map.geolocation.userPosition().done(function(pos, id) {
                    //     var ori = Map.latLng(pos.latitude, pos.longitude);
                    //     var optRoute = Map.options.route(latLng, ori);
                    //     Map.direction.route(optRoute);
                    //     navigator.geolocation.clearWatch(id);
                    //     // Map.geolocation.watch(false);
                    // }, function(err) {
                    //     // app.toast('Tidak dapat mendapatkan posisi sekarang');
                    //     // console.log(err);
                    //     var pos = Map.geolocation.getSavePosition();
                    //     var ori = Map.latLng(pos.lat, pos.lng);
                    //     var optRoute = Map.options.route(latLng, ori);
                    //     Map.direction.route(optRoute);
                    // });
                } else {
                    app.toast("Peta Tidak Dapat Ditampilkan");
                }
            },
            loadDetailPage: function(data) {
                var self = this;
                $('#detailwisata .wisata-info,#detailwisata .wisata-map').html('');
                $('#detailwisata .wisata-info').load('page/detail/wisata.html', function(e) {
                    self.setGambar(data.gambar);
                    self.setInfo(data);
                    self.setTiket(data.tikets);
                    self.setFasilitas(data.fasilitas);
                    self.setRateAndReview(data.tempatable_id);
                    $('.tab-review').data('wisata_id', data.tempatable_id);
                    $('#detailwisata .wisata-info').trigger('create');
                });
            },
            init: function(data) {
                var info = data[0] || {};
                var gambars = data[1] || [];
                var tikets = data[2] || [];
                var fasilitas = data[3] || [];

                this.setGambar(gambars);
                this.setInfo(info);
                this.setTiket(tikets);
                this.setFasilitas(fasilitas);
                this.setRateAndReview(info.tempatable_id);
                $('.tab-review').data('wisata_id', info.tempatable_id);
                page.tabMap.data = info;
                // this.setUserReview(info.tempatable_id);
            },
            setGambar: function(gambars) {
                var swiper = $('#detailwisata .swiper-container .swiper-wrapper');
                var dom = '';
                var self = this;
                if (gambars.length === 0) {
                    dom += self.domGambar('404.png');
                } else {
                    for (var i = 0; i < gambars.length; i++) {
                        dom += self.domGambar(gambars[i].nama);
                    }
                }
                swiper.html(dom);
                self.swiper();
            },
            setInfo: function(data) {
                $('#list-info-wisata').listview({
                    defaults: true,
                    create: function(e) {
                        for (var key in data) {
                            $('#value-' + key).html(data[key]);
                        }
                    }
                });
            },
            setFasilitas: function(fasilitas) {
                var self = this;
                var dom = '';
                if (fasilitas.length)
                    $('#daftar-fasilitas').show();
                else
                    $('#daftar-fasilitas').hide();
                for (var i = 0; i < fasilitas.length; i++) {
                    dom += self.domFasilitas(fasilitas[i]);
                }
                $('#list-fasilitas').html(dom);
                $('#list-fasilitas').listview({
                    defaults: true
                });
            },
            setTiket: function(tikets) {
                var self = this;
                var dom = '';
                if (tikets.length) {
                    $('#daftar-tiket').show();
                    $('#value-is_tiket').html('Ada ' + tikets.length + ' jenis tiket');
                } else {
                    $('#daftar-tiket').hide();
                    $('#value-is_tiket').html('Tidak Ada tiket');
                }
                for (var i = 0; i < tikets.length; i++) {
                    dom += self.domTiket(tikets[i]);
                }
                $('#list-tiket').html(dom);
                $('#list-tiket').listview({
                    defaults: true
                });
            },
            setRateAndReview: function(wisata_id) {
                var self = this;
                var user = app.getUser();

                $('.proses-user-rating').show();
                $('.form-rating').hide();
                server.send('rate', {
                    wisata_id: wisata_id,
                    user_id: user.id,
                }, function(data, msg, xhr) {
                    data.rate = data.rate || 0;
                    data.review = data.review || 0;
                    self.onFinishUserReview(data.review, wisata_id);
                    self.onFinishRateTempat(data);
                }, function(e) {
                    app.toast("Tidak dapat memuat rating tempat ini");
                    $('.proses-user-rating').hide();
                    $('#hasil-rating').removeClass('proses-rating');
                });
            },
            onProsesUserReview: function() {
                $('.proses-user-rating').show();
                $('.form-rating').hide();
            },
            onFinishUserReview: function(data, wisata_id) {
                if (data) {
                    this.setUserReview(data);
                } else {
                    $('.form-rating').show();
                }

                this.setFormUserReview(data, wisata_id);
                $('.proses-user-rating').hide();
            },
            onTambahUserReview: function(data) {
                // if (page.detailwisata.rateYo2) {
                var hasil = parseFloat(data.rate).toFixed(1);
                $('#hasil-rating h2').html(hasil);
                page.detailwisata.rateYo2.rateYo('rating', data.rate);
                // }
            },
            onFinishRateTempat: function(data) {
                var hasil = parseFloat(data.rate).toFixed(1);
                $('#current-rating h2').html(hasil);
                page.detailwisata.rateYo2 = $("#rateYo2").rateYo({
                    rating: data.rate,
                    halfStar: true,
                    readOnly: true
                });
                $('#hasil-rating').removeClass('proses-rating');
            },
            // setUserReview: function(wisata_id) {
            //     var self = this;
            //     var wisata = {};
            //     wisata.wisata_id = wisata_id;
            //     query.userReview().done(function(data) {
            //         if (data) {
            //             self.setUserReview(data);
            //         } else {
            //             self.setFormUserReview(wisata);
            //         }
            //     });
            // },
            setFormUserReview: function(data, wisata_id) {
                data = data || {};
                data.rating = data.rating || 0;
                var user_id = app.getUser().id || 0;
                $('#form-rating :input[name="user_id"]').val(user_id);
                $('#form-rating :input[name="rating"]').val(data.rating);
                $('#form-rating :input[name="wisata_id"]').val(wisata_id || data.wisata_id);
                $('#form-rating :input[name="judul"]').val(data.judul);
                $('#form-rating :input[name="review"]').val(data.review);
                $('#form-rating :input[name="id"]').val(data.id);
                $('.review-hapus').addClass('hidden');
                $('.review-hapus').data('id', data.id);
                this.rateYo = $("#rateYo").rateYo({
                    rating: data.rating,
                    starWidth: "40px",
                    fullStar: true,
                    spacing: "10px",
                    onSet: function(rating, rateYoInstance) {
                        var form = $('#form-rating');
                        $('#form-rating input[name="rating"]').val(rating);
                        if (rating > 0) {
                            form.show();
                        } else {
                            form.hide();
                        }
                    },
                });
            },
            resetFormUserReview: function() {
                $('#form-rating :input[name="judul"]').val('');
                $('#form-rating :input[name="review"]').val('');
            },
            setUserReview: function(data) {
                var dom = this.domUserReview(data);
                $('.sudah-rating ul li').html(dom);
                $('.sudah-rating ul').listview({
                    default: true
                });
                $('.sudah-rating').show();
            },
            setAllReview: function(wisata_id) {

            },
            domUserReview: function(data) {
                var nama = '<h3>' + app.getUser().name + '</h3>';
                var star = '';
                for (var i = 0; i < data.rating; i++) {
                    star += '<i class="zmdi zmdi-star"></i>';
                }
                var tgl = new Date(data.created_at.replace(/-/g, "/"));
                return nama +
                    '<b class="ui-li-aside"><a href="#"  id="edit-user-review" class="review-edit"><i class="zmdi zmdi-edit"></i>Edit</a></b>' +
                    '<p>' + tgl.getDate() + '/' + parseInt(tgl.getMonth() + 1) + '/' + tgl.getFullYear() + '  ' +
                    star +
                    '</p>' +
                    '<b>' + data.judul + '</b>' +
                    '<p>' + data.review + '</p>';
            },
            domTiket: function(tiket) {
                return '<li>' +
                    '<p>' + tiket.nama + '</p>' +
                    '<strong>Rp. ' + tiket.harga + '</strong>' +
                    '<b> pada ' + tiket.operasional + '</b>' +
                    '</li>';
            },
            domFasilitas: function(fas) {
                return '<li>' +
                    '<i class="zmdi zmdi-check zmdi-hc-fw"></i><strong> ' + fas.nama + ' </strong>' +
                    '</li>';
            },
            domGambar: function(nama) {
                return '<div class="swiper-slide">' +
                    '<img data-src="' + server.url + 'img/large/' + nama + '" class="swiper-lazy">' +
                    '   <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>' +
                    '</div>';
            },
            swiper: function() {
                this.objswiper = new Swiper('#detailwisata .swiper-container', {
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    // Disable preloading of all images
                    preloadImages: false,
                    autoplay: 3000,
                    effect: 'slide',
                    // Enable lazy loading
                    lazyLoading: true
                });
            },
            onClickReviewCancel: function() {
                if ($('.review-hapus').hasClass('hidden')) {
                    $('#form-rating').hide();
                    page.detailwisata.rateYo.rateYo('rating', 0);
                } else {
                    $('.sudah-rating').show();
                    $('.form-rating,#form-rating').hide();
                }
            },
            onClickReviewEdit: function() {
                $('.sudah-rating').hide();
                $('.form-rating,#form-rating').show();
                $('.review-hapus').removeClass('hidden');
            },
            onClickReviewHapus: function(e) {
                var self = page.detailwisata;
                var id = $(this).data('id');

                self.onProsesUserReview();
                server.hapusReview(id).done(function(data) {
                    $('.review-hapus').addClass('hidden');
                    $('#form-rating').hide();
                    $('.form-rating').show();
                    self.rateYo.rateYo('rating', 0);
                    $('.proses-user-rating').hide();
                    self.resetFormUserReview();
                    self.onTambahUserReview(data);
                });
            },
        },
        tabReview: {
            init: function(event) {
                var wisata_id = $(this).data('wisata_id');
                var self = page.tabReview;
                self.prosesReviews();
                server.reviews(wisata_id).done(function(data, msg, xhr) {
                    self.appendReviews(data);
                }).fail(function(xhr) {
                    console.log('terjadi kesalahan');
                });
            },
            appendReviews: function(data) {
                var dom = '';
                if (data.length) {
                    for (var i = 0; i < data.length; i++) {
                        dom += this.domReview(data[i]);
                    }
                    $('#tab-review ul.ui-listview').html(dom);
                    $('#tab-review ul.ui-listview').listview('refresh');
                } else {
                    $('#tab-review ul.ui-listview').html('<p>Tidak ada review</p>');
                }
            },
            prosesReviews: function() {
                var proses = '<div class="proses-reviews">' +
                    '<i class="zmdi zmdi-rotate-right zmdi-hc-spin zmdi-hc-2x"></i> ' +
                    '<p>loading...</p>' +
                    '</div>';
                $('#tab-review ul.ui-listview').html(proses);
            },
            domReview: function(data) {
                var star = '';
                for (var i = 0; i < data.rating; i++) {
                    star += '<i class="zmdi zmdi-star"></i></a>';
                }
                var tgl = new Date(data.updated_at.replace(/-/g, "/"));
                return '<li>' +
                    '<h3>' + data.user.name + '</h3>' +
                    '<p>' + tgl.getDate() + '/' + parseInt(tgl.getMonth() + 1) + '/' + tgl.getFullYear() + ' ' +
                    star +
                    '</p>' +
                    '<b>' + data.judul + '</b>' +
                    '<p>' + data.review + '</p>' +
                    '</li>';
            },
        },
        tabMap: {
            init: function() {
                if (Map.init && page.tabMap.data) {
                    var opt = Map.options.map();
                    var data = page.tabMap.data;
                    var latLng = Map.latLng(data.latitude, data.longitude);
                    opt.center = latLng;
                    opt.zoom = 18;
                    Map.init('detail_canvas', opt);
                    Map.marker.add(latLng, data);
                    page.tabMap.route(latLng);
                } else {
                    app.toast("Peta Tidak Dapat Ditampilkan");
                }
            },
            route: function(latLng) {
                var opt = Map.options.route(latLng);
                console.log(opt);
                Map.direction.route(opt);
                Map.geolocation.watch(false);
            }

        }
    };