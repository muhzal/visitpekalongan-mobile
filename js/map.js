var _map;
var ds;
var dr;
var curr;
var optMap = {
                  center : new google.maps.LatLng(41.850033, -87.6500523),
                  zoom   : 6,
                  mapTypeId : google.maps.MapTypeId.ROADMAP,
                  zoomControlOptions : {
                        'position' : google.maps.ControlPosition.LEFT_CENTER, 
                        'style' : google.maps.ZoomControlStyle.SMALL
                  },      
                  disableDoubleClickZoom : true,
            };
var optRute = {
        origin: "Blackpool", 
        destination: "Blackburn", 
        // waypoints: [{
        //   location: "Preston",
        //   stopover:true}],
        optimizeWaypoints: true,
        // provideRouteAlternatives :  true,
        travelMode: google.maps.DirectionsTravelMode.WALKING
};
var optGeol = {
      enableHighAccuracy : true,
      timeout : Infinity,
      maximumAge : 10000,
};
function initMap(){
  ds = new google.maps.DirectionsService();
  dr = new google.maps.DirectionsRenderer();
  _map = new google.maps.Map(document.getElementById('mapview'), optMap);
  dr.setMap(_map);  
  prosesRute(optRute);
  navigator.geolocation.getCurrentPosition(geolSuccess, geolError, optGeol);
}

function geolSuccess(pos) {
       console.log(pos);       
}
function geolError(e) {
       console.log("Gagal mendapatkan lokasi karena :"+e.message);       
}

function prosesRute(optRute) {
      ds.route(optRute, handleRute);
}

function handleRute(response, status){
      console.log(response);
      if (status == google.maps.DirectionsStatus.OK) {
            var route = response.routes[0];
            dr.setDirections(response);
            hitungJarakTotal(response);
            displaySumary(route);
            displaySteps(response);
      }else{
            alert('directionsService status: '+status);
      }
      
}

var routePanel = document.getElementById('route-list');
function setRouteList(route){
            return "<li><h3>From :"+route.start_address+"</h3>"+
                  "<h3>To : "+route.end_address+"</h3>"+
                  "<p class='ui-li-aside'><strong>"+route.distance.text+"</strong></p></li>";
}
function displaySumary(route){
      for (var i = 0; i < route.legs.length; i++) {
            $('#route-list').append(setRouteList(route.legs[i]));
      }
      $('#route-list').listview('refresh');
}

function hitungJarakTotal(routes) {
       var totalJarak = 0;
       var totalWaktu = 0;
       var route      = routes.routes[0];
       for (var i = 0; i < route.legs.length; i++) {
             totalJarak += route.legs[i].distance.value;
             totalWaktu  += route.legs[i].duration.value;
       } 
       totalJarak = totalJarak/1000;    
       var jam = parseInt(totalWaktu/3600);
       var menit = parseInt((totalWaktu - jam*3600)/60);
       var dom =  '<li data-role="list-divider">Jarak Total : '+totalJarak+', Waktu Tempuh : '+jam+':'+menit+'</li>';
       $('#route-list').prepend(dom);
}
function displaySteps(response){
      var legs = response.routes[0].legs;
      var dom =  '<li data-role="list-divider">Langkah - langkah</li>';
      $('#route-list').append(dom);
      for (var i = 0; i < legs.length; i++) {
            var steps = legs[i].steps;
            for (var j = 0; j < steps.length; j++) {
                  $('#route-list').append(listSteps(steps[j]));
            }
      }
      $('#route-list').listview('refresh');
}
function listSteps(a) {
      return "<li><p>"+a.instructions+"</p>"+
                  "<p>Sekitar "+a.distance.text+"</p>"+
                  "<p class='ui-li-aside'>waktu <strong>"+a.duration.text+"</strong></p></li>";
}