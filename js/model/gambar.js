function Gambar(data){
	this.id = data.id || 9999;
	this.nama = data.nama || '';
	this.tempat_id = data.tempat_id || 9999;	
}

Gambar.prototype = {
	constructor : Gambar,
	query 		: {
			create 	: 'create table if not exists gambars ('+
						'id int primary key not null,'+
						'nama char(20),'+
						'tempat_id int not null,'+
						'FOREIGN KEY(tempat_id) REFERENCES tempats(id))',						
			all 	: 'select * from gambars',						
			tempat 	: 'select * from gambars where tempat_id= ?',
			insert 	:	'insert into gambars(id,nama,tempat_id) values(?,?,?)', 
	},
	createTable : function(){
		var def = $.Deferred();
		dao.executeSql(this.query.create,[],function(){
			def.resolve();
		});
		return def;
	},
	save : function(){
		dao.transaction(this.query.insert,[this.id,this.nama,this.tempat_id],function(){
			console.log('gambars inserted');
		});
	},
	tempat : function(tempat_id,callback){
		dao.executeSql(this.query.tempat,[tempat_id], function(data){
			callback(data);
		});
	}
}
window.gambar = new Gambar({});

