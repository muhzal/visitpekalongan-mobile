window.Tempat = function(data){	
			if (data !== null) {		
					this.id 				= data.id || 0;
				    this.nama 				= data.nama || '';
				    this.alamat 			= data.alamat || '';
				    this.latitude 			= data.latitude || 0;
				    this.longitude 			= data.longitude || 0;
				    this.keterangan 		= data.keterangan || '';		    		    
				    this.tempatable_id 		= data.tempatable_id || 0;
				    this.tempatable_type 	= data.tempatable_type || '';
				    this.created_at 		= data.created_at || '2015-01-01 00:00:00';
				    this.updated_at			= data.updated_at || '2015-01-01 00:00:00';
				    this.gambar();
			}
			this.tableName 			= 'tempats';
			this.column	   = ['id', 'nama', 'alamat', 'latitude', 'longitude','keterangan','tempatable_id','tempatable_type','created_at','updated_at'];
			this.query	   = {
				'create'	: 'create table if not exists '+this.tableName+'(' +
								'id int primary key not null,'+
				                'nama char(100),'+
				                'alamat char(100),'+
				                'latitude real,'+
				                'longitude real,'+
				                'keterangan TEXT,'+
				                'tempatable_id int,'+
				                'tempatable_type char(100),'+
				                'updated_at DATETIME,'+
				                'created_at DATETIME)',					                   
				'all'	 	: 'select * from ' + this.tableName,
				'insert' 	: 'insert into '+this.tableName+' ('+this.column.join(',')+') values(?,?,?,?,?,?,?,?,?,?)',
				'delete' 	: 'delete FROM '+this.tableName+' where id = ?',
				'update' 	: 'update FROM '+this.tableName+' set name=?, email=?, token=? where id=?',
				'find'		: 'select * FROM '+this.tableName+' where id=?',
				'wisata'	: 'select * from tempats,gambars,wisatas where '+
								'tempats.id = gambars.tempat_id and tempats.tempatable_type="App\Models\Wisata"',
			};		
		this.createTable = function(){
			var def = $.Deferred();
			dao.executeSql(this.query.create,[],function(){
				def.resolve();
			});
			return def;
		};
		this.gambar = function(id){
			var q = 'select id,nama from gambars where tempat_id=?';
			var def = $.Deferred();
			var result = [];
			dao.executeSql(q,[id],function(data){
				for (var i = 0; i < data.rows.length; i++) {
					result.push(data.rows.item(i));
				}
				def.resolve(result);
			});
			return def.promise();
		};
		this.insert = function(data){
			var x = [];
			dao.insert(this.query.insert, data, function(r){
				for(var i = 0; i < r.rows.length;i++){
					x.push(r.rows.item(i));
				}
			});		
			console.log(x);
			return x;
		};
		this.all = function(){
			var def = $.Deferred();
			var result = [];
			dao.executeSql(this.query.all,[],function(data){
				for (var i = 0; i < data.rows.length; i++) {
					result.push(data.rows.item(i));
				}
				def.resolve(result);
			});
			return def.promise();
		};
		this.wisata = function(){
			var def = $.Deferred();
			var result = [];
			dao.executeSql(this.query.wisata,[],function(data){
				for (var i = 0; i < data.rows.length; i++) {
					result.push(data.rows.item(i));
				}
				def.resolve(result);
			});
			return def.promise();
		};
		this.find = function(id){
			var result;
			dao.select(this.query.find,function(data){		          
				result = data.rows.item(0);		          
			});
			return result;
		};
		this.save = function(){
			dao.insert(this.query.insert,[this.id,this.nama,
			                              this.alamat,
			                              this.latitude,
			                              this.longitude,
			                              this.keterangan,
			                              this.tempatable_id,
			                              this.tempatable_type,
			                              this.created_at,
			                              this.updated_at],function(r){
				console.log('saved tempat');
				console.log(r);
			});		
		}
	}
window.tempat = new Tempat(null);