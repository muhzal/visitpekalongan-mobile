window.User = {	
		init : function(){
			this.tableName = 'users';
			this.column	   = ['id', 'name', 'email', 'token'];			
			this.query	   = {
				'create'	: 'create table if not exists '+this.tableName+'(' +
								'id int primary key not null,'+
				                'name char(100),'+
				                'email char(100) NOT NULL,'+
				                'token TEXT)',					                   
				'all'	 	: 'select * from ' + this.tableName,
				'insert' 	: 'insert into '+this.tableName+' ('+this.column.join(',')+') values(?,?,?)',
				'hapus' 	: 'delete FROM '+this.tableName,
				'update' 	: 'update FROM '+this.tableName+' set name=?, email=?, token=? where id=?',
				'token'		: 'update FROM '+this.tableName+' set token=? where id=?',
				'find'		: 'select * FROM '+this.tableName+' where id=?',
			};
		},
		createTable : function(){			
			var def = $.Deferred();
			dao.executeSql(User.query.create,[],function(){
				def.resolve();
			});
			return def;
		},
		insert : function(data){			
			dao.db.transaction(
	                function(tx) {
	                    tx.executeSql(User.query.hapus,[],function(db,r){
	                    	console.log(r);
	                    	console.log('user terhapus');
	                    });
	                    tx.executeSql(User.query.insert,data,function(db,r){
	                    	console.log(r);
	                    	console.log('user ditambahkan');
	                    },dao.txErrorHandler);
	                },
	                function(r){
	                	console.log(r);
	                	console.log('insert user selesai');
	                },dao.txErrorHandler);
		},
		getUser : function(callback){
			dao.select(User.query.all,function(data){
		          return callback(data.rows.item(i))
			});
		},
		all : function(){
			var res;
			dao.select(User.query.all,function(data){		          
				res = data.rows.item(0);					
			});
			console.log(res);
			return res;
		},
		getToken : function(){
			return window.localStorage.getItem('token');
		},
		setToken : function(token){
			window.localStorage.setItem('token',token);			
		},
		save : function(){
			var x = [this.id, this.name,this.email];
			this.insert(x);
		}
		
	}

User.init();