window.server = {
    url: "http://app.visitpekalongan.id/",
    // url: "http://adm.kalongcation.xyz/",
    // url: "http://localhost/laravel/public/",
    // url: "http://192.168.137.1/laravel/public/",
    send: function(action, data, fnDone, fnFail) {
        var token = app.getToken();
        var url = server.url + 'mobile/' + action;
        if (token || action === 'login' || action === 'signup' || action === 'reset') {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                headers: {
                    'Authorization': 'Bearer ' + token,
                },
            }).done(fnDone).fail(fnFail).always(function(data, txt, xhr) {
                if (xhr.getResponseHeader && xhr.getResponseHeader('Authorization')) {
                    app.setToken(xhr);
                } else if (data.getResponseHeader && data.getResponseHeader('Authorization')) {
                    app.setToken(data);

                }
                if (data.status === 401 || data.status === 400) {
                    // page.change('login');
                    page.loader.hide('login');
                }
            });

        } else {
            page.loader.hide('login');
        }
    },
    signup: function(data) {
        $('.ui-error').removeClass('ui-error');
        $('#signup .error-card ul').html(' ');
        $('#signup .error-card ').hide();
        this.send('signup', data, server.onLoginSuccess, server.onSignupGagal);
        page.loader.show();
    },
    onSignupGagal: function(xhr, textStatus, errorThrown) {
        var response = xhr.responseJSON;
        console.log(response);
        var msg = '';
        for (var name in response) {
            $('#form-signup input[name="' + name + '"]').parent().addClass('ui-error');
            for (var i = 0; i < response[name].length; i++) {
                msg += '<li>' + response[name][i] + '</li>';
            }
        }
        if (response === null) {
            msg = '<li>' + xhr.statusText + '</li>';
        }
        $('#signup .error-card ul').html(msg);
        $('#signup .error-card ').show();
        page.loader.hide();
    },
    login: function(data) {
        $('.ui-error').removeClass('ui-error');
        $('#login .error-card ul').html(' ');
        $('#login .error-card ').hide();
        this.send('login', data, server.onLoginSuccess, server.onLoginGagal);
        page.loader.show();
    },
    onLoginSuccess: function(data, message, jqhr) {
        if (jqhr.status === 200 && data.token && data.user) {
            var temp = new model.user(data.user);
            query.deleteUser().done(function() {
                temp.save();
            });
            $('#signup :input,#login :input').not(':hidden').val('');
            // window.localStorage.setItem('user_id', temp.id);
            // window.localStorage.setItem('user_name', temp.name);
            // window.localStorage.setItem('user_email', temp.email);
            window.localStorage.setItem('user', JSON.stringify(temp));
            app.setToken(data.token);
            // if (window.localStorage.getItem('installed')) {
            //     page.loader.hide('home');
            // } else {
            server.cekUpdate();
            // }
        } else {
            app.toast('terjadi kesalahan, coba lagi');
            page.loader.hide('login');
        }
    },
    onLoginGagal: function(xhr, textStatus, errorThrown) {
        // console.log(e);
        // app.toast('terjadi kesalahan, coba lagi');
        // page.loader.hide('login'); 
        var response = xhr.responseJSON;
        // console.log(xhr);
        var msg = '';
        for (var name in response) {
            $('#form-login input[name="' + name + '"]').parent().addClass('ui-error');
            for (var i = 0; i < response[name].length; i++) {
                msg += '<li>' + response[name][i] + '</li>';
            }
        }
        if (response === null) {
            msg = '<li>' + xhr.statusText + '</li>';
        }
        $('#login .error-card ul').html(msg);
        $('#login .error-card ').show();
        page.loader.hide();
        // page.loader.hide('home');
    },
    getLastUpdate: function() {
        return window.localStorage.getItem('lastUpdate');
    },
    setLastUpdate: function(a) {
        return window.localStorage.setItem('lastUpdate', a);
    },
    getLastUpdateDate: function() {
        return window.localStorage.getItem('lastUpdateDate');
    },
    setLastUpdateDate: function(a) {
        return window.localStorage.setItem('lastUpdateDate', a);
    },
    cekUpdate: function() {        
        var user_id = app.getUser();
        dao.getLastUpdate().done(function(res) {
            server.dateUpdate = res;
            server.send('cekUpdate', {
                'data_latest': res,
                'user_id': user_id ? user_id.id : 0,
            }, server.onCekUpdateSukses, server.onCekUpdateGagal);
        });
    },
    onCekUpdateSukses: function(data, message, jqhr) {
        if (jqhr.status === 200 && data.tempat) {
            server.installUpdate(data);
        } else if (jqhr.status == 401 || jqhr.satatus == 400) {
            page.loader.hide('login');
        } else {
            // app.toast(jqhr.responseText);
            page.loader.hide('home');
        }
    },
    installUpdate: function(data) {
        console.log('install Update');
        var tempat = data.tempat || [];
        var def;
        def = this.saveUpdate(tempat);
        // page.loader.hide('home');
        $.when.apply($, def).then(function() {
                // app.toast('data berhasil di simpan');
                page.loader.hide('home');
            })
            .fail(function() {
                app.toast('Terjadi kesalahan saat menyimpan data');
                page.loader.hide('home');
            });
    },
    saveUpdate: function(data) {
        var def = [],
            tempat, gambar, tiket;
        $.each(data, function(a, v) {
            tempat = new model.tempat(v);
            def.push(tempat.save());
            def.push(tempat.tempatable.save());
            $.each(tempat.gambars, function(i, v) {
                gambar = new model.gambar(v);
                def.push(gambar.save());
            });
            if (tempat.tempatable.tikets) {
                $.each(tempat.tempatable.tikets, function(i, v) {
                    tiket = new model.tiket(v);
                    def.push(tiket.save());
                });
            }
            // console.log(tempat.tempatable.fasilitaswisata);
            if (tempat.tempatable.fasilitaswisata) {
                var fasilitas;
                $.each(tempat.tempatable.fasilitaswisata, function(i, v) {
                    fasilitas = new model.fasilitaswisata(v);
                    def.push(fasilitas.save());
                });
            }
        });
        return def;
    },
    onCekUpdateGagal: function(e) {
        console.log(e.getAllResponseHeaders());

        if (e.status == 401 || e.status == 400 || typeof server.dateUpdate == "undefined") {
            page.loader.hide('login');
        } else if (e.status == 501 && server.tryUpdate != 3) {
            server.tryUpdate = server.tryUpdate ? server.tryUpdate + 1 : 1;
            server.cekUpdate();
        } else {
            page.loader.hide('home');
        }
    },
    tambahReview: function(review) {
        //(action, review, fnDone, fnFail
        var dw = page.detailwisata;
        console.log('TAMBAH REVIEW');
        dw.onProsesUserReview();
        server.send('tambahreview', review,
            function(data, msg, xhr) {
                dw.onFinishUserReview(data);
                dw.onTambahUserReview(data.wisata);
            },
            function(xhr, msg, err) {

            });
    },
    hapusReview: function(id) {
        var def = $.Deferred();
        var data = {
            id: id,
        };
        server.send('hapusreview', data,
            function(data, msg, xhr) {
                def.resolve(data);
            },
            function(xhr, msg, err) {
                def.reject(xhr);
            });
        return def.promise();
    },
    reviews: function(wisata_id) {
        console.log(wisata_id);
        var data = {
            'wisata_id': wisata_id,
        };
        var def = $.Deferred();
        server.send('reviews', data, function(data, msg, xhr) {
            def.resolve(data, msg, xhr);
        }, function(xhr) {
            def.reject(xhr);
        });
        return def.promise();
    },
};