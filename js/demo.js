$(document).ready(function() {
	$("#listview-panel-kiri").listview();
	$("#panel-kiri").panel();
	loader.hide();
	page.change('home');
});

var loader = {
	show: function() {
		$('body').removeClass('loaded');
	},
	hide: function() {
		$('body').addClass('loaded');
	},
};

var server = {
	login: function() {
		
	}
};

var page = {
	change: function(page) {
		$(":mobile-pagecontainer").pagecontainer("change", "#" + page, {
			showLoadMsg: false
		});
	},
}