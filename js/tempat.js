var Tempat = {
	listTempat: function(jenis) {
		var self = this;
		var content = $('#list-tempat');
		self.listHeader(jenis);
		content.attr('data-jenis', jenis);
		var dom = '';
		self.getDataList(jenis).done(function(data) {
			if (data.length === 0) {
				content.html('<center style="margin-top:20px;" class="nd2-title"><i>Anda belum menambahkan tempat</i></center>');
				page.loader.hide('daftar-tempat');
			} else {

				self.hitungJarak(data).done(function(result) {
					for (var i = 0; i < result.length; i++) {
						dom += self.listDom(result[i]);
					}
					content.html(dom);
					content.listview({
						default: true
					});
					content.listview('refresh');
					page.loader.hide('daftar-tempat');
				});
			}
		}).fail(function(err) {
			console.log(err);
		});
	},
	getDataList: function(jenis) {
		var def = $.Deferred();
		var query;
		if (typeof jenis === 'number') {
			query = this.query.wisata(jenis);
		} else {
			query = this.query[jenis] ? this.query[jenis]() : this.query.default(jenis);
		}
		dao.db.readTransaction(function(tx) {
			tx.executeSql(query, [], function(tx2, results) {
				// console.log(results.rows);
				var hasil = [];
				for (var i = 0; i < results.rows.length; i++) {
					hasil.push(results.rows.item(i));
				}
				def.resolve(hasil);
			}, function(x, y) {
				def.reject(y);
				console.log(y);
			});
		});
		return def.promise();
	},
	query: {
		favorite: function() {
			return 'SELECT tempats.*,gambars.tempat_id, gambars.nama as gambar ' +
				'FROM tempats left join gambars on tempats.id=gambars.tempat_id ' +
				'where tempats.favorite = 1 GROUP by tempats.id order by tempats.id DESC';
		},
		tempatku: function() {
			return "SELECT tempats.*,gambars.nama as gambar FROM tempats left join gambars on gambars.tempat_id=tempats.id  where tempats.user_id =" + app.getUser().id + "  GROUP by tempats.id";
		},
		semuawisata: function() {
			return "SELECT `tempats`.`id`,`gambars`.`tempat_id`,`tempats`.`tempatable_id`,`tempats`.`longitude`,`tempats`.`latitude`, `tempats`.`nama`, `tempats`.`alamat`,`tempats`.`keterangan`, `gambars`.`nama` as gambar FROM `tempats` , `wisatas` left join gambars on gambars.tempat_id = tempats.id where wisatas.id = tempats.tempatable_id GROUP by wisatas.id";
		},
		wisata: function(a) {
			return "SELECT `tempats`.`id`,`tempats`.`tempatable_id`,`tempats`.`longitude`,`tempats`.`latitude`, `tempats`.`nama`, `tempats`.`alamat`," +
				"`tempats`.`keterangan`, `gambars`.`nama` as gambar,`gambars`.`tempat_id` FROM `tempats` , `wisatas`" +
				" left join gambars on tempats.id = gambars.tempat_id where wisatas.id = tempats.tempatable_id and wisatas.jeniswisata_id = " + a + " and gambars.tempat_id = tempats.id GROUP by wisatas.id";
		},
		default: function(jenis) {
			return 'SELECT tempats.*, `gambars`.`nama` as gambar,`gambars`.`tempat_id` ' +
				'FROM `tempats` , `' + jenis + 's` ' +
				' left join gambars on tempats.id = gambars.tempat_id where tempats.publish = 1 and tempats.tempatable_type like "%' + jenis + '" and gambars.tempat_id=tempats.id GROUP by tempats.id';
		}
	},
	listHeader: function(jenis) {
		$('#daftar-tempat header h1').html('Daftar ' + jenis);
	},
	hitungJarak: function(data) { //data object dari database                
		var def = $.Deferred();
		Map.distance.proses(data).done(function(temp) {
			// console.log(temp);
			temp.sort(function(a, b) {
				return parseFloat(a.jarak) - parseFloat(b.jarak);
			});
			def.resolve(temp);
		}).fail(function(e) {
			def.reject(e);
		});
		return def.promise();
	},
	listDom: function(data) {
		// console.log(data);
		data.jarak = data.jarak || 0;
		var gambar = data.gambar === null ? '404.png' : data.gambar;
		var fav = data.favorite ? 'zmdi-favorite' : 'zmdi-favorite-outline';
		var jenis = data.tempatable_type ? data.tempatable_type.split('\\')[2].toLowerCase() : null;
		var url = jenis === 'wisata' ? '#detailwisata' : '#detailtempat';
		return '<li>' +
			'<a href="' + url + '" data-id="' + data.id + '" data-jenis="' + jenis + '"class="ui-btn waves-effect waves-button waves-effect waves-button"  data-transition="slide">' +
			'<img src="' + server.url + 'img/small/' + gambar + '"  width="100px" height="100px" class="ui-thumbnail ui-thumbnail-circular" />' +
			'<h2>' + data.nama + '</h3><p>' + data.alamat + '<br>' + // data.keterangan +
			'</p>' +
			'<p><strong>' + parseFloat(data.jarak).toFixed(2) + '</strong> km</p>' +
			'</a>' +
			'<button class="clr-red ui-li-aside ui-mini ui-btn-transparent"><i class="zmdi ' + fav + '"></i></button>' +
			'</li>';
	},
	domGambar: function(nama) {
		return '<div class="swiper-slide">' +
			'<img data-src="' + server.url + 'img/large/' + nama + '" class="swiper-lazy">' +
			'   <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>' +
			'</div>';
	},
	swiper: function() {
		this.objswiper = new Swiper('.swiper-container', {
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			pagination: '.swiper-pagination',
			paginationClickable: true,
			// Disable preloading of all images
			preloadImages: false,
			autoplay: 3000,
			effect: 'slide',
			// Enable lazy loading
			lazyLoading: true
		});
	},
	detail: function(id, jenis) {
		var self = this;
		$('#detailtempat .tab-info').click();
		page.loader.show();
		self.getDataDetail(id, jenis).done(function(data) {
			$('.tempat-map.nd2Tabs-content-tab').html('<div id="tempat-map"></div>');
			$('#detailtempat .btn-favorite').data({
				'id': id,
				'favorite': data.favorite
			});
			self.detailTitle(data);
			self.loadDetail(data, jenis);
			self.currentData = data;
			// $('#detailtempat .btn-favorite');
			if (data.favorite) {
				$('#detailtempat .btn-favorite .zmdi').addClass('zmdi-favorite').removeClass('zmdi-favorite-outline').parent().addClass('clr-btn-red').removeClass('clr-btn-green');
			} else {
				$('#detailtempat .btn-favorite .zmdi').removeClass('zmdi-favorite').addClass('zmdi-favorite-outline').parent().addClass('clr-btn-green').removeClass('clr-btn-red');
			}
			$(document).one('click', '.tempat-map', Tempat.initMap);
			// self.initMap(data);
		}).fail(function(error) {

		});
	},
	initMap: function() {
		var data = Tempat.currentData || {};
		if (typeof google !== 'undefined') {
			var optMap = Map.options.map();
			var latLng = Map.latLng(data.latitude, data.longitude);
			var height = window.screen.height;
			var header = $("#detailtempat header").height();
			$('#tempat-map').height(height - header);
			optMap.center = latLng;
			optMap.zoom = 19;
			Map.init('tempat-map', optMap);
			Map.marker.create(latLng, data).setMap(Map.map);

			var pos = Map.geolocation.getSavePosition();
			if (pos.lat || pos.lng) {
				var ori = Map.latLng(pos.lat, pos.lng);
				var optRoute = Map.options.route(latLng, ori);
				Map.direction.route(optRoute);
				Map.marker.me(pos);
				Map.marker.currentMarker.setMap(Map.map);
			}
			// page.tabMap.route(latLng);
			// Map.geolocation.userPosition().done(function(pos, id) {
			// 	var ori = Map.latLng(pos.coords.latitude, pos.coords.longitude);
			// 	var optRoute = Map.options.route(latLng, ori);
			// 	Map.direction.route(optRoute);
			// 	navigator.geolocation.clearWatch(id);
			// 	// Map.geolocation.watch(false);
			// }, function(err) {
			// 	// app.toast('Tidak dapat mendapatkan posisi sekarang');
			// 	console.log(err);
			// 	var pos = Map.geolocation.getSavePosition();
			// 	var ori = Map.latLng(pos.lat, pos.lng);
			// 	var optRoute = Map.options.route(latLng, ori);
			// 	Map.direction.route(optRoute);
			// });
		} else {
			app.toast("Peta Tidak Dapat Ditampilkan");
		}
	},
	getDataDetail: function(id, jenis) {
		var def = $.Deferred(),
			jenisSukses,
			tempat = {},
			gambar = [],
			obJenis = {},
			q1 = 'SELECT * from ' + jenis + 's join tempats on tempats.id=' + jenis + 's.tempat_id where tempats.id=' + id,
			q2 = "SELECT * from gambars where tempat_id=" + id,
			q3 = 'select * from ' + jenis + 's where tempat_id=' + id;

		dao.db.readTransaction(function(tx) {
			tx.executeSql(q1, [], function(tx2, results) {
				tempat = results.rows.length > 0 ? results.rows.item(0) : {};
				// console.log(results.rows);
				// console.log(q1);
			});
			tx.executeSql(q2, [], function(tx2, results) {
				// console.log(results.rows);
				for (var i = 0; i < results.rows.length; i++) {
					gambar.push(results.rows.item(i));
				}
			});
			// tx.executeSql(q3, [], function(tx2, results) {
			// 	console.log(q3);
			// 	obJenis = results.rows.length > 0 ? results.rows[0] : {};
			// });
		}, function(error) {
			//fail function
			console.log(error);
			def.reject(error);
		}, function() {
			//sukses function
			// $.extend(true, tempat, gambar);
			tempat.gambar = gambar;
			// tempat.tempatbale = obJenis;
			// $.extend(true, tempat, obJenis);
			// console.log(tempat);
			def.resolve(tempat);
		});
		return def.promise();
	},
	detailTitle: function(data) {
		$('#detailtempat .judul-tempat').html('Detail ' + data.nama);
	},
	loadDetail: function(data, jenis) {
		$('#detailtempat .info').html('');
		$('#detailtempat .info').load('page/detail/' + jenis + '.html', function() {
			$('#list-info-tempat').listview({
				defaults: true,
				create: function(e) {
					console.log(data);
					var gambar = '';
					var swiper = $('.swiper-container .swiper-wrapper');
					for (var key in data) {
						$('.value-' + key).html(data[key]);
					}
					if (data.gambar.length === 0) {
						gambar += Tempat.domGambar('404.png');
					} else {
						for (var i = 0; i < data.gambar.length; i++) {
							gambar += Tempat.domGambar(data.gambar[i].nama);
						}
					}
					swiper.html(gambar);
					Tempat.swiper();
					page.loader.hide();
					$('#detailtempat .tab-info').click();
				}
			});
		});
	},
	loadFormTambah: function(jenis) {
		$('#content-form-tambah').html(' ');
		$('#content-form-tambah').load('page/form/' + jenis + '.html', function(e) {
			var self = this;
			$('#form-tambah header h1').html('Tambah ' + jenis);
			Tempat.fileFoto = [];
			Tempat.tikets = [];
			$('.selectize').selectize({
				plugins: ['remove_button'],
				sortField: 'text',
				// delimiter: ', ',
				create: true,
				onInitialize: function() {
					$('.selectize-control input').attr('data-role', 'none');
					$(self).trigger('create');
				}
			});
		});
	},
	initMapTambah: function() {
		$('#map-tambah .ui-content').html('<div id="form-map"></div>');
		if (typeof google !== 'undefined') {
			var optMap = Map.options.map();
			var height = window.screen.height;
			var header = $("#map-tambah header").height();
			var latLng = {};
			Tempat.formMarker = null;
			$('a[href="#form-tambah"]').addClass('ui-disabled');
			$('#form-map').height(height - header);
			Map.init('form-map', optMap);
			Map.geolocation.userPosition().done(function(pos, id) {
				latLng = Map.latLng(pos.coords.latitude, pos.coords.longitude);
				Map.geolocation.savePosition(pos);
				navigator.geolocation.clearWatch(id);
			}).fail(function(err) {
				var pos = Map.geolocation.getSavePosition();
				latLng = Map.latLng(pos.lat, pos.lng);
			}).always(function(pos, id) {
				if (latLng.lat() !== 0 && latLng.lng() !== 0 && Tempat.formMarker === null) {
					Map.map.setCenter(latLng);
					Tempat.formMarker = Map.marker.create(latLng);
					Tempat.formMarker.setMap(Map.map);
					$('a[href="#form-tambah"]').removeClass('ui-disabled');
				}
			});
			google.maps.event.addListener(Map.map, 'click', function(e) {
				if (Tempat.formMarker === null) {
					Tempat.formMarker = Map.marker.create(e.latLng);
					Tempat.formMarker.setMap(Map.map);
					$('a[href="#form-tambah"]').removeClass('ui-disabled');
				}
				Tempat.formMarker.setPosition(e.latLng);
			});
		}
	},
	fileFoto: [],
	tikets: [],
	foto: function(type) {
		var self = this;
		navigator.camera.getPicture(function(imageURI) {
			var name = new Date().getTime();
			var dom =
				'<div class="image-item col-xs-4">' +
				'<button type="button" data-name=' + name + ' class="ui-btn clr-btn-red btn-hapus-foto ui-btn-inline">x</button>' +
				'<img src="data:image/jpeg;base64,' + imageURI + '" alt="" width="150">' +
				'</div>';
			$('.image-panel').append(dom);

			var foto = {
				name: name,
				value: imageURI,
			};
			self.fileFoto.push(foto);
		}, function(err) {
			console.log(err);
		}, {
			quality: 50,
			destinationType: Camera.DestinationType.DATA_URL,
			mediaType: Camera.MediaType.PICTURE,
			sourceType: type,
			targetHeight: 1000,
			targetWidth: 1500,
			encodingType: Camera.EncodingType.JPEG,
		});
	},
	submitTempat: function(data) {
		page.loader.show();
		server.send('tambahTempat', data, function(respon, txt, xhr) {
			// console.log(respon);
			server.saveUpdate([respon]);
			// page.loader.hide('home');
			$('a[href="#daftar-tempat"].tempatku').click();
			app.toast('Data Berhasil ditambahkan');

		}, function(xhr) {
			console.log(xhr);
			page.loader.hide();
		});
	},
	collectData: function(form) {
		var data = $(form).serializeArray();
		var i;
		data.push({
			name: 'tempat[latitude]',
			value: Tempat.formMarker.position.lat(),
		});
		data.push({
			name: 'tempat[longitude]',
			value: Tempat.formMarker.position.lng(),
		});
		data.push({
			name: 'tempat[user_id]',
			value: app.getUser().id,
		});
		for (i = 0; i < Tempat.fileFoto.length; i++) {
			data.push({
				name: 'gambar[' + i + ']',
				value: Tempat.fileFoto[i].value,
			});
		}
		for (i = 0; i < Tempat.tikets.length; i++) {
			data.push({
				name: 'tikets[' + i + '][nama]',
				value: Tempat.tikets[i].nama,
			});
			data.push({
				name: 'tikets[' + i + '][operasional]',
				value: Tempat.tikets[i].hari,
			});
			data.push({
				name: 'tikets[' + i + '][harga]',
				value: Tempat.tikets[i].harga,
			});
		}
		// console.log(data);
		return data;
	},
	addTiket: function(e) {
		var tiket = {};
		tiket.nama = $('#nama-tiket').val();
		tiket.harga = $('#harga-tiket').val();
		tiket.hari = $('#hari-tiket').val();
		var dom = '<li>' +
			'<strong> - ' + tiket.nama + ': Rp. ' + tiket.harga + ' - ' + tiket.hari + ' </strong> <button type = "button"' +
			'class = "ui-mini ui-btn ui-btn-inline tiket-delete ui-btn-right"' +
			' data-nama=' + tiket.nama +
			' data-hari=' + tiket.hari +
			' data-harga=' + tiket.harga +
			'> x </button>' +
			'</li>';
		var cek = Tempat.isEmpty(tiket);
		if (cek) {
			$('#' + cek + '-tiket').focus();
		} else {
			if (Tempat.tikets.length === 0) {
				$('.daftar-tiket ul').html('');
			}
			Tempat.tikets.push(tiket);
			$('.daftar-tiket ul').append(dom).listview('refresh');
			$('#nama-tiket').val('');
			$('#harga-tiket').val('');
			$('#hari-tiket').val('');
		}
	},
	isEmpty: function(obj) {
		for (var key in obj) {
			if (obj[key] === '') {
				return key;
			}
		}
		return false;
	},
	changeTiket: function() {
		console.log($(this).val() === '1');
		if ($(this).val() === '1') {
			$('.tiket-panel').show();
		} else {
			$('.tiket-panel').hide();
		}
	},
	deleteTiket: function(e) {
		var nama = $(this).data('nama');
		var harga = $(this).data('harga');
		var hari = $(this).data('hari');
		// var temp = Tempat.tikets;

		var index = $.grep(Tempat.tikets, function(data, i) {
			if (data.nama !== nama && data.harga !== harga && data.hari !== hari) {
				return i;
			}
			return false;
		});
		console.log(index.length);
		if (index) {
			Tempat.tikets.splice(index, 1);
			$(this).parent().remove();
		}

		// var count =Tempat.tikets.length;
		if (Tempat.tikets.length === 0) {
			$('.daftar-tiket ul').html('<center>Tidak Ada Tiket</center>').listview('refresh');
		}
	}
};